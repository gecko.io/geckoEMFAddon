/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.repository;

import org.gecko.emf.osgi.compare.repository.impl.EMFCompareRepository;

/**
 * Annotation for the {@link EMFCompareRepository} configuration.
 * @author Mark Hoffmann
 * @since 02.12.2019
 */
public @interface CompareRepositoryConfiguration {
	
	enum RepositoryType {
		PROTOTYPE,
		SINGLETON
	}
	
	/*
	 * The name of the repository
	 */
	String name();
	/*
	 * The target filter for the delegate repository
	 */
	String repository_target();
	/*
	 * The target filter for the model object merger
	 */
	String merger_target();
	/*
	 * The target filter for the comparison factory
	 */
	String comparisonFactory_target();
	/*
	 * Optional target filter for model object merger visitors
	 */
	String visitor_target();
	/*
	 * Type for the repository
	 */
	RepositoryType type() default RepositoryType.PROTOTYPE;

}
