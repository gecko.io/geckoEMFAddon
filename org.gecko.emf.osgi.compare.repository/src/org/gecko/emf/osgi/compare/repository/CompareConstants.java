/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.repository;

import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * Interface with constants
 * @author Mark Hoffmann
 * @since 02.12.2019
 */
public interface CompareConstants {
	
	public static final String COMPARE_CONFIGURATION_FACTORY_ID = "CompareRepositoryConfiguration";
	public static final String COMPARE_FACTORY_ID = "CompareRepository";
	public static final String REPOSITORY_TYPE = "repoType";
	public static final String COMPARE_TYPE = "compare";
	public static final String REPOSITORY_DELEGATE = "delegateRepository";
	public static final String REPOSITORY_DELEGATE_TARGET = "delegateRepository.target";
	public static final String MERGER_DELEGATE = "delegateMerger";
	public static final String MERGER_TARGET = "delegateMerger.target";
	
	/**
	 * Property key to additionally provide an ID for the object,
	 * in case none is given to the object.
	 * This may be needed for getting an existing instance from the repository to
	 * compare against.
	 * The expected value is of type {@link String}
	 */
	public static final String COMPARE_ID = "compare.id";
	
	/**
	 * Property key to enforce saving, even if there is no change detected.
	 * This can be helpful, if some ignore features are given to the compare process.
	 * The expected value is of type {@link Boolean}
	 */
	public static final String COMPARE_FORCE_SAVE = "compare.forceSave";
	
	/**
	 * Property key to provide a list of {@link EStructuralFeature} to be ignored for the comparison.
	 * The expected value is of type {@link List}
	 */
	public static final String COMPARE_IGNORE_FEATURES = "compare.ignoreFeature";

}
