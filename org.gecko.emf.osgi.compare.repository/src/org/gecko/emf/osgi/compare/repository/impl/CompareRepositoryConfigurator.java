/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.repository.impl;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.gecko.emf.osgi.compare.repository.CompareConstants;
import org.gecko.emf.osgi.compare.repository.CompareRepositoryConfiguration;
import org.gecko.emf.repository.EMFRepository;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * Configurator for the {@link EMFCompareRepository}
 * @author Mark Hoffmann
 * @since 03.12.2019
 */
@Component(immediate = true, service = CompareRepositoryConfigurator.class, configurationPolicy = ConfigurationPolicy.REQUIRE, configurationPid = CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID)
public class CompareRepositoryConfigurator implements CompareConstants {
	
	private static final Logger logger = Logger.getLogger(CompareRepositoryConfigurator.class.getName());
	@Reference
	private ConfigurationAdmin configAdmin;
	private Configuration mergerConfig;
	private Configuration repositoryConfig;
	private String name = null;
	
	@Activate
	public void activate(CompareRepositoryConfiguration configuration, Map<String, Object> properties) {
		name  = configuration.name();
		if (name == null) {
			name = (String) properties.get(Constants.SERVICE_PID);
			name = name.split("~")[1];
		}
		try {
			configureModelObjectMerger(name, configuration);
			configureCompareRepository(name, configuration);
		} catch (Exception e) {
			logger.log(Level.SEVERE, String.format("[%s] Error configuring EMFCompareRepository", name), e);
		}
	}
	
	@Deactivate
	public void deactivate() {
		if (mergerConfig != null) {
			try {
				mergerConfig.delete();
				mergerConfig = null;
			} catch (IOException e) {
				logger.log(Level.SEVERE, String.format("[%s] Error deleting model object merger configuration", name), e);
			}
		}
		if (repositoryConfig != null) {
			try {
				repositoryConfig.delete();
				repositoryConfig = null;
			} catch (IOException e) {
				logger.log(Level.SEVERE, String.format("[%s] Error deleting model EMFCompareRepository configuration", name), e);
			}
		}
		name = null;
	}
	
	/**
	 * Configures the {@link EMFCompareRepository}
	 * @param name the name of the configuration
	 * @param configuration the configuration
	 * @throws IOException 
	 */
	private void configureCompareRepository(String name, CompareRepositoryConfiguration configuration) throws IOException {
		String repoTarget = configuration.repository_target();
		String momTarget = configuration.merger_target();
		if (momTarget == null) {
			momTarget = String.format("(%s=%s)", ModelObjectMerger.MERGER_NAME, name);
		}
		if (repositoryConfig == null) {
			repositoryConfig = configAdmin.getFactoryConfiguration(COMPARE_FACTORY_ID, name, "?");
		}
		Dictionary<String, Object> repositoryProps = new Hashtable<>();
		repositoryProps.put(EMFRepository.PROP_ID, name);
		repositoryProps.put(REPOSITORY_TYPE, COMPARE_TYPE);
		repositoryProps.put(MERGER_TARGET, momTarget);
		if (validateFilter(repoTarget)) {
			repositoryProps.put(REPOSITORY_DELEGATE_TARGET, repoTarget);
		}
		if (CompareRepositoryConfiguration.RepositoryType.PROTOTYPE.equals(configuration.type())) {
			repositoryProps.put(Constants.SERVICE_SCOPE, Constants.SCOPE_PROTOTYPE);
		}
		repositoryConfig.update(repositoryProps);
	}

	/**
	 * Configures the ModelObjectMerger service
	 * @param name the name of the repository
	 * @param configuration the configuration
	 * @throws IOException
	 */
	private void configureModelObjectMerger(String name, CompareRepositoryConfiguration configuration) throws IOException {
		String comparisonTarget = configuration.comparisonFactory_target();
		if (comparisonTarget == null) {
			comparisonTarget = "(type=default)";
		}
		String visitorTarget = configuration.visitor_target();
		if (mergerConfig == null) {
			mergerConfig = configAdmin.getFactoryConfiguration(ModelObjectMerger.CONFIGURABLE_FACTORY_ID, name, "?");
		}
		Dictionary<String, Object> mergerProps = new Hashtable<>();
		mergerProps.put(ModelObjectMerger.MERGER_NAME, name);
		if (validateFilter(visitorTarget)) {
			mergerProps.put(ModelObjectMerger.VISITOR_REFERENCE + ".target", visitorTarget);
		}
		if (validateFilter(comparisonTarget)) {
			mergerProps.put(ModelObjectMerger.COMPARISON_REFERENCE + ".target", comparisonTarget);
		}
		mergerConfig.update(mergerProps);
	}
	
	/**
	 * Validates if the target filter string is a valid LDAP filter
	 * @param filterString the filter string to validate as LDAP filter
	 * @return <code>true</code>, if validation was successful, otherwise <code>false</code>
	 */
	private boolean validateFilter(String filterString) {
		if (filterString == null || filterString.isEmpty()) {
			return false;
		}
		try {
			filterString = filterString.replace("\\(", "(").replace("\\)", ")");
			FrameworkUtil.createFilter(filterString);
			return true;
		} catch (InvalidSyntaxException e) {
			logger.log(Level.SEVERE, String.format("Cannot parse target filter '%s'", filterString), e);
		}
		return false;
	}

}
