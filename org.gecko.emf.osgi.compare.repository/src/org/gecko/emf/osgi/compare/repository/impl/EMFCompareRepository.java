/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.repository.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.gecko.emf.osgi.compare.api.ModelCompareResultType;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.gecko.emf.osgi.compare.repository.CompareConstants;
import org.gecko.emf.repository.DelegateEMFRepository;
import org.gecko.emf.repository.EMFRepository;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentServiceObjects;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

@Component(configurationPolicy = ConfigurationPolicy.REQUIRE, configurationPid = CompareConstants.COMPARE_FACTORY_ID, service = EMFRepository.class)
public class EMFCompareRepository extends DelegateEMFRepository {
	
	private static Logger logger = Logger.getLogger(EMFCompareRepository.class.getName());
	@Reference(name = CompareConstants.MERGER_DELEGATE)
	private ModelObjectMerger merger;
	@Reference(name = CompareConstants.REPOSITORY_DELEGATE)
	private ComponentServiceObjects<EMFRepository> delegateCSO;
	private String name = null;
	
	@Activate
	public void activate(Map<String, Object> config) throws ConfigurationException {
		name = (String) config.get(EMFRepository.PROP_ID);
		if (name == null) {
			throw new ConfigurationException("name", "A name property must be given for this factory");
		}
		EMFRepository delegate = delegateCSO.getService();
		setDelegateRepository(delegate);
	}
	
	@Deactivate
	public void deactivate() {
		EMFRepository delegate = getDelegateRepository();
		if (delegate != null) {
			delegateCSO.ungetService(delegate);
		}
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.repository.EMFRepository#getId()
	 */
	@Override
	public String getId() {
		return name;
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.repository.DelegateEMFRepository#save(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public void save(EObject object) {
		compareAndSave(object, null, null);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.repository.DelegateEMFRepository#save(org.eclipse.emf.ecore.EObject, java.util.Map)
	 */
	@Override
	public void save(EObject object, Map<?, ?> options) {
		compareAndSave(object, options, null);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.repository.DelegateEMFRepository#save(org.eclipse.emf.ecore.EObject, java.lang.String)
	 */
	@Override
	public void save(EObject object, String contentType) {
		compareAndSave(object, null, contentType);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.repository.DelegateEMFRepository#save(org.eclipse.emf.ecore.EObject, java.lang.String, java.util.Map)
	 */
	@Override
	public void save(EObject object, String contentType, Map<?, ?> options) {
		compareAndSave(object, options, contentType);
	}
	
	private void compareAndSave(EObject object, Map<?, ?> options, String contentType) {
		if (object == null) {
			logger.warning("Cannot compare a null instance");
			return;
		}
		Map<?, ?> properties;
		if (options != null) {
			properties = new HashMap<>(options);
		} else {
			properties = new HashMap<>();
		}
		List<EStructuralFeature> ignoreFeature = getIgnoreFeatures(properties);
		String sourceId = (String) properties.get(CompareConstants.COMPARE_ID);
		EClass objectClass = object.eClass();
		EObject existing = null;
		if (objectClass.getEIDAttribute() != null) {
			// Options map has a higher priority, than the information from the object
			if (sourceId == null) {
				sourceId = EcoreUtil.getID(object);
			}
		}
		if (sourceId != null) {
			existing = getEObject(objectClass, sourceId, properties);
		}
		// We expect to have a new object 
		if (existing == null) {
			doSave(object, options, contentType);
			return;
		}
		ModelCompareResultType resultType = merger.compareAndMerge(object.eResource(), existing.eResource(), ignoreFeature);
		switch (resultType) {
		case COMPARE_ERROR:
			logger.log(Level.SEVERE, String.format("[%s] Error during compare of EObject", sourceId));
			return;
		case MERGE_ERROR:
			logger.log(Level.SEVERE, String.format("[%s] Error during merge of EObject", sourceId));
			return;
		default:
			if (Boolean.TRUE.equals(properties.get(CompareConstants.COMPARE_FORCE_SAVE))) {
				doSave(existing, options, contentType);
				break;
			} else if (ModelCompareResultType.MERGE_SUCCESS.equals(resultType)) {
				doSave(existing, options, contentType);
				break;
			}
			break;
		}
		if (ModelCompareResultType.MERGE_SUCCESS.equals(resultType)) {
			merger.fireVisitorChanges(existing.eResource());
		}
	}
	
	/**
	 * Return the {@link List} with {@link EStructuralFeature} to be ignored or an empty {@link List}
	 * @param properties properties to extract the information from
	 * @return the list with {@link EStructuralFeature}
	 */
	private List<EStructuralFeature> getIgnoreFeatures(Map<?, ?> properties) {
		Object ifObj = properties.get(CompareConstants.COMPARE_IGNORE_FEATURES);
		if (ifObj != null && ifObj instanceof List) {
			return ((List<?>)ifObj).stream().filter(o->o instanceof EStructuralFeature).map(o->(EStructuralFeature)o).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	/**
	 * Delegates the save to the corresponding method in the underlying delegate
	 * @param toSave the object to save
	 * @param options the options, can be <code>null</code>
	 * @param contentType the content type, can be <code>null</code>
	 */
	private void doSave(EObject toSave, Map<?, ?> options, String contentType) {
		if (toSave == null) {
			logger.warning("There is nothing to be saved");
			return;
		}
		if (options == null) {
			if (contentType == null) {
				super.save(toSave);
			} else {
				super.save(toSave, contentType);
			}
		} else {
			if (contentType == null) {
				super.save(toSave, options);
			} else {
				super.save(toSave, contentType, options);
			}
		}
	}
	
}
