package org.gecko.emf.osgi.compare.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.emf.osgi.compare.api.ModelCompareResultType;
import org.gecko.emf.osgi.compare.api.ModelDiffVisitor;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.gecko.emf.osgi.model.test.GenderType;
import org.gecko.emf.osgi.model.test.Person;
import org.gecko.emf.osgi.model.test.TestFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;

@RunWith(MockitoJUnitRunner.class)
public class CompareIntegrationTest extends AbstractOSGiTest {

	@Mock
	private ModelDiffVisitor visitor;
	
	/**
	 * Creates a new instance.
	 */
	public CompareIntegrationTest() {
		super(FrameworkUtil.getBundle(CompareIntegrationTest.class).getBundleContext());
	}
	
	@Test
	public void testExampleDiffEngine() throws InterruptedException {
		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		ModelObjectMerger merger = mergerChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(merger);
		
		ServiceChecker<ResourceSetFactory> rsfChecker = createTrackedChecker(ResourceSetFactory.class);
		ResourceSetFactory rsf = rsfChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		
		ModelCompareResultType compareResult = merger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);
	}
	
	@Test
	public void testDiffVisitor() throws InterruptedException {
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("type", "default");
		registerServiceForCleanup(ModelDiffVisitor.class, visitor, properties);
		
		ServiceChecker<ModelDiffVisitor> visitorChecker = createTrackedChecker(ModelDiffVisitor.class);
		ModelDiffVisitor mdv = visitorChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(mdv);
		assertEquals(visitor, mdv);
		
		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		ModelObjectMerger merger = mergerChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(merger);
		
		ServiceChecker<ResourceSetFactory> rsfChecker = createTrackedChecker(ResourceSetFactory.class);
		ResourceSetFactory rsf = rsfChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(rsf);
		
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		
		ModelCompareResultType compareResult = merger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);
		
		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, times(1)).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
		merger.fireVisitorChanges(r1);
		
		verify(visitor, times(1)).finishedVisiting(any(Resource.class));
		verify(visitor, times(1)).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
	}
	
	@Test
	public void testDiffVisitorMissingTarget() throws InterruptedException {
		
		registerServiceForCleanup(ModelDiffVisitor.class, visitor, new Hashtable<String, Object>());
		
		ServiceChecker<ModelDiffVisitor> visitorChecker = createTrackedChecker(ModelDiffVisitor.class);
		
		ModelDiffVisitor mdv = visitorChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(mdv);
		assertEquals(visitor, mdv);
		
		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		ModelObjectMerger merger = mergerChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(merger);
		
		ServiceChecker<ResourceSetFactory> rsfChecker = createTrackedChecker(ResourceSetFactory.class);
		ResourceSetFactory rsf = rsfChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(rsf);
		
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		
		ModelCompareResultType compareResult = merger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);
		
		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
		merger.fireVisitorChanges(r1);
		
		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doBefore()
	 */
	@Override
	public void doBefore() {
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doAfter()
	 */
	@Override
	public void doAfter() {
	}

}