package org.gecko.emf.osgi.compare.tests;

import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IEqualityHelperFactory;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;

@RunWith(MockitoJUnitRunner.class)
public class DefaultComponentsIntegrationTest extends AbstractOSGiTest {

	/**
	 * Creates a new instance.
	 */
	public DefaultComponentsIntegrationTest() {
		super(FrameworkUtil.getBundle(DefaultComponentsIntegrationTest.class).getBundleContext());
	}
	
	@Test
	public void testDefaultComponentsExist() throws InterruptedException, InvalidSyntaxException {
		ServiceChecker<IEqualityHelperFactory> equalityChecker = createTrackedChecker(IEqualityHelperFactory.class);
		equalityChecker.assertCreations(1, true);
		
		ServiceChecker<IComparisonFactory> comparisonChecker = createTrackedChecker(IComparisonFactory.class);
		comparisonChecker.assertCreations(1, true);
		
		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker("(component.name=EMFModelObjectMerger)", true);
		mergerChecker.assertCreations(1, true);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doBefore()
	 */
	@Override
	public void doBefore() {
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doAfter()
	 */
	@Override
	public void doAfter() {
	}

}