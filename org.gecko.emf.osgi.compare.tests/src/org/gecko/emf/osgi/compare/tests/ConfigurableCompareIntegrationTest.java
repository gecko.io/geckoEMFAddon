package org.gecko.emf.osgi.compare.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.MatchResource;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.emf.osgi.compare.api.ModelCompareResultType;
import org.gecko.emf.osgi.compare.api.ModelDiffVisitor;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.gecko.emf.osgi.model.test.GenderType;
import org.gecko.emf.osgi.model.test.Person;
import org.gecko.emf.osgi.model.test.TestFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurableCompareIntegrationTest extends AbstractOSGiTest {

	@Mock
	private ModelDiffVisitor visitor;
	@Mock
	private IComparisonFactory comparisonFactory;
	@Mock
	private Comparison comparison;

	/**
	 * Creates a new instance.
	 */
	public ConfigurableCompareIntegrationTest() {
		super(FrameworkUtil.getBundle(ConfigurableCompareIntegrationTest.class).getBundleContext());
	}

	@Test
	public void testConfigurationNoValidProperty() throws InterruptedException, IOException {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("bla", "blub");
		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		mergerChecker.assertCreations(1, true);
		createConfigForCleanup(ModelObjectMerger.FACTORY_ID, "custom", "?", properties);
		mergerChecker.assertCreations(1, true);
	}

	@Test
	public void testConfigurationNoName() throws InterruptedException, IOException {
		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		mergerChecker.assertCreations(1, true);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("visitorTarget", "dum");
		Configuration config = createConfigForCleanup(ModelObjectMerger.FACTORY_ID, "custom", "?", properties);
		ServiceChecker<?> configChecker = getServiceCheckerForConfiguration(config);
		configChecker.assertCreations(0, true);
	}

	@Test
	public void testConfigurationName() throws InterruptedException, IOException, InvalidSyntaxException {
		ServiceChecker<IComparisonFactory> comparisonChecker = createTrackedChecker(IComparisonFactory.class);
		comparisonChecker.assertCreations(1, true);

		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		mergerChecker.assertCreations(1, true);
		
		ServiceChecker<?> configChecker = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=custom)", false);
		configChecker.assertCreations(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "custom");
		createConfigForCleanup(ModelObjectMerger.FACTORY_ID, "custom", "?", properties);
		configChecker.assertCreations(1, true);
		ServiceReference<?> configMergerRef = configChecker.getTrackedServiceReference();
		assertEquals("custom", configMergerRef.getProperty(ModelObjectMerger.MERGER_NAME));
	}

	@Test
	public void testConfigurationNoValidVisitorTarget() throws InterruptedException, IOException, InvalidSyntaxException {
		ServiceChecker<IComparisonFactory> comparisonChecker = createTrackedChecker(IComparisonFactory.class);
		comparisonChecker.assertCreations(1, true);

		Dictionary<String, Object> diffProperties = new Hashtable<String, Object>();
		diffProperties.put("type", "haha");
		registerServiceForCleanup(ModelDiffVisitor.class, visitor, diffProperties);
		
		ServiceChecker<ModelDiffVisitor> visitorChecker = createTrackedChecker(ModelDiffVisitor.class);
		visitorChecker.assertCreations(1, true);
		ModelDiffVisitor mdv = visitorChecker.getTrackedService();
		assertNotNull(mdv);
		assertEquals(visitor, mdv);

		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		mergerChecker.assertCreations(1, true);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "custom");
		
		ServiceChecker<?> configChecker = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=custom)", false);
		configChecker.assertCreations(0, false);
		
		createConfigForCleanup(ModelObjectMerger.FACTORY_ID, "custom", "?", properties);

		ServiceReference<?> configMergerRef = configChecker.assertCreations(1, true).getTrackedServiceReference();
		assertEquals("custom", configMergerRef.getProperty(ModelObjectMerger.MERGER_NAME));

		ModelObjectMerger merger = mergerChecker.getTrackedService();
		assertNotNull(merger);

		ServiceChecker<ResourceSetFactory> rsfChecker = createTrackedChecker(ResourceSetFactory.class);
		ResourceSetFactory rsf = rsfChecker.assertCreations(1, true).getTrackedService();
		assertNotNull(rsf);

		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);

		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);

		ModelCompareResultType compareResult = merger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);

		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));

		merger.fireVisitorChanges(r1);

		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
		ModelObjectMerger configMerger = (ModelObjectMerger) configChecker.getTrackedService();
		assertNotNull(configMerger);
		
		compareResult = merger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);
		
		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
		merger.fireVisitorChanges(r1);
		
		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));

	}
	
	@Test
	public void testConfigurationVisitorTarget() throws InterruptedException, IOException, InvalidSyntaxException {
		ServiceChecker<IComparisonFactory> comparisonChecker = createTrackedChecker(IComparisonFactory.class);
		comparisonChecker.assertCreations(1, true);
		
		Dictionary<String, Object> diffProperties = new Hashtable<String, Object>();
		diffProperties.put("type", "haha");
		registerServiceForCleanup(ModelDiffVisitor.class, visitor, diffProperties);
		
		ServiceChecker<ModelDiffVisitor> visitorChecker = createTrackedChecker(ModelDiffVisitor.class);
		visitorChecker.assertCreations(1, true);
		ModelDiffVisitor mdv = visitorChecker.getTrackedService();
		assertNotNull(mdv);
		assertEquals(visitor, mdv);
		
		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		mergerChecker.assertCreations(1, true);
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "custom");
		properties.put("visitorTarget", "(type=haha)");
		
		createConfigForCleanup(ModelObjectMerger.FACTORY_ID, "custom", "?", properties);
		/*
		 * we cannot take the original properties here, because there is a component in
		 * between that is responsible to create the config for the reals service. We want to track this
		 * real services
		 */
		ServiceChecker<?> configChecker = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=custom)", false);
		configChecker.assertCreations(1, true);
		
		ModelObjectMerger configMerger = (ModelObjectMerger) configChecker.getTrackedService();
		assertNotNull(configMerger);
		
		ModelObjectMerger merger = mergerChecker.getTrackedService();
		assertNotNull(merger);
		assertNotEquals(configMerger, merger);
		
		ServiceChecker<ResourceSetFactory> rsfChecker = createTrackedChecker(ResourceSetFactory.class);
		rsfChecker.assertCreations(1, true);
		ResourceSetFactory rsf = rsfChecker.getTrackedService();
		assertNotNull(rsf);
		
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		
		ModelCompareResultType compareResult = merger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);
		
		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
		merger.fireVisitorChanges(r1);
		
		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
		compareResult = configMerger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);
		
		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, times(1)).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
		configMerger.fireVisitorChanges(r1);
		
		verify(visitor, times(1)).finishedVisiting(any(Resource.class));
		verify(visitor, times(1)).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));
		
	}
	
	@Test
	public void testConfigurationComparisonTarget() throws InterruptedException, IOException, InvalidSyntaxException {
		ServiceChecker<IComparisonFactory> comparisonChecker = createTrackedChecker(IComparisonFactory.class);
		comparisonChecker.assertCreations(1, true);
		
		Dictionary<String, Object> diffProperties = new Hashtable<String, Object>();
		diffProperties.put("type", "haha");
		registerServiceForCleanup(IComparisonFactory.class, comparisonFactory, diffProperties);
		Mockito.when(comparisonFactory.createComparison()).thenReturn(comparison);
		Mockito.when(comparison.getMatchedResources()).thenReturn(new BasicEList<MatchResource>());
		Mockito.when(comparison.getMatches()).thenReturn(new BasicEList<Match>());
		
		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		mergerChecker.assertCreations(1, true);
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "custom");
		properties.put("comparisonFactoryTarget", "(type=haha)");
		
		createConfigForCleanup(ModelObjectMerger.FACTORY_ID, "custom", "?", properties);
		/*
		 * we cannot take the original properties here, because there is a component in
		 * between that is responsible to create the config for the reals service. We want to track this
		 * real services
		 */
		ServiceChecker<?> configChecker = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=custom)", false);
		configChecker.assertCreations(1, true);
		
		ModelObjectMerger configMerger = (ModelObjectMerger) configChecker.getTrackedService();
		assertNotNull(configMerger);
		
		ModelObjectMerger merger = mergerChecker.getTrackedService();
		assertNotNull(merger);
		assertNotEquals(configMerger, merger);
		
		ServiceChecker<ResourceSetFactory> rsfChecker = createTrackedChecker(ResourceSetFactory.class);
		rsfChecker.assertCreations(1, true);
		ResourceSetFactory rsf = rsfChecker.getTrackedService();
		assertNotNull(rsf);
		
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		
		ModelCompareResultType compareResult = merger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);
		
		verify(comparisonFactory, never()).createComparison();
		try {
			compareResult = configMerger.compare(r2, r1);
			fail("Compare must fail, because cannot mock everything :-)");
		} catch (Exception e) {
			
		}
		verify(comparisonFactory, times(1)).createComparison();
		
	}

	@Test
	public void testService() throws InvalidSyntaxException {
		ServiceChecker<Object> checker = createTrackedChecker("(&(name=hello)(target=\\(hello=world\\)))", false);
		checker.assertCreations(0, true);
		
		Dictionary<String, Object> props = new Hashtable<String, Object>();
		props.put("name", "hello");
		props.put("target", "(hello=world)");
		registerServiceForCleanup(Object.class, new String("test"), props);
		checker.assertCreations(1, true);
	}
	
	@Test
	public void testDiffVisitorMissingTarget() throws InterruptedException {

		registerServiceForCleanup(ModelDiffVisitor.class, visitor, new Hashtable<String, Object>());

		ServiceChecker<ModelDiffVisitor> visitorChecker = createTrackedChecker(ModelDiffVisitor.class);
		visitorChecker.assertCreations(1, true);

		ModelDiffVisitor mdv = visitorChecker.getTrackedService();
		assertNotNull(mdv);
		assertEquals(visitor, mdv);

		ServiceChecker<ModelObjectMerger> mergerChecker = createTrackedChecker(ModelObjectMerger.class);
		mergerChecker.assertCreations(1, true);
		ModelObjectMerger merger = mergerChecker.getTrackedService();
		assertNotNull(merger);

		ServiceChecker<ResourceSetFactory> rsfChecker = createTrackedChecker(ResourceSetFactory.class);
		rsfChecker.assertCreations(1, true);
		ResourceSetFactory rsf = rsfChecker.getTrackedService();
		assertNotNull(rsf);

		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);

		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);

		ModelCompareResultType compareResult = merger.compare(r2, r1);
		assertEquals(ModelCompareResultType.COMPARE_SUCCESS, compareResult);

		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));

		merger.fireVisitorChanges(r1);

		verify(visitor, never()).finishedVisiting(any(Resource.class));
		verify(visitor, never()).visitAttributeChange(any(AttributeChange.class));
		verify(visitor, never()).visitReferenceChange(any(ReferenceChange.class));

	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doBefore()
	 */
	@Override
	public void doBefore() {
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doAfter()
	 */
	@Override
	public void doAfter() {
	}

}