/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.repository.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.gecko.emf.osgi.compare.repository.CompareConstants;
import org.gecko.emf.repository.EMFRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.PrototypeServiceFactory;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;

/**
 * <p>
 * Integration tests for the EMFCompareRepository
 * </p>
 * 
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class RepositoryConfigurationIntegrationTest extends AbstractOSGiTest {
	
	@Mock
	private EMFRepository repository;
	
	/**
	 * Creates a new instance.
	 * @param bundleContext
	 */
	public RepositoryConfigurationIntegrationTest() {
		super(FrameworkUtil.getBundle(RepositoryConfigurationIntegrationTest.class).getBundleContext());
	}

	/**
	 * Here you can put everything you want to be exectued before every test
	 */
	public void doBefore() {
		setupMockServices();
		
	}
	
	/**
	 * Here you can put everything you want to be exectued after every test
	 */
	public void doAfter() {
	}
	
	/**
	 * Here you can put your test
	 * @throws IOException 
	 * @throws InvalidSyntaxException 
	 */
	@Test
	public void testRepositoryFailWithNoName() throws IOException, InvalidSyntaxException {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("dummy", "dummy");
		
		ServiceChecker<Object> mergerSC = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=abc)", false)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=abc)(repoType=compare))", false)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Configuration c = createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "abc", "?", properties);
		
		ServiceReference<?> reference = mergerSC
				.assertCreations(1, true)
				.assertRemovals(0, false)
				.getTrackedServiceReference();
		assertEquals("abc", reference.getProperty(ModelObjectMerger.MERGER_NAME));
		repoSC.assertCreations(1, true).assertRemovals(0, false);
		
		ModelObjectMerger merger = (ModelObjectMerger) mergerSC.getTrackedService();
		assertNotNull(merger);
		
		deleteConfigurationAndRemoveFromCleanup(c);
		mergerSC.assertCreations(1, false).assertRemovals(1, true);
		repoSC.assertCreations(1, false).assertRemovals(1, true);
	}
	
	@Test
	public void testRepositoryWithName() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> mergerSC = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=dummy)", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy)(repoType=compare))", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy");
		Configuration c = createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		ServiceReference<?> reference = mergerSC
				.assertCreations(1, true)
				.assertRemovals(0, false)
				.getTrackedServiceReference();
		assertEquals("dummy", reference.getProperty(ModelObjectMerger.MERGER_NAME));
		ModelObjectMerger merger = (ModelObjectMerger) mergerSC.getTrackedService();
		assertNotNull(merger);
		
		repoSC.assertCreations(1, true).assertRemovals(0, false);
		
		deleteConfigurationAndRemoveFromCleanup(c);
		mergerSC.assertCreations(1, false).assertRemovals(1, true);
		repoSC.assertCreations(1, false).assertRemovals(1, true);
	}
	
	@Test
	public void testRepositoryWrongRepoTarget() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> mergerSC = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=dummy2)", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy2)(repoType=compare))", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy2");
		properties.put("repository.target", "(test=hallo)");
		Configuration c = createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		mergerSC.assertCreations(1, true).assertRemovals(0, false);
		repoSC.assertCreations(0, true).assertRemovals(0, false);
		
		deleteConfigurationAndRemoveFromCleanup(c);
		mergerSC.assertCreations(1, false).assertRemovals(1, true);
		repoSC.assertCreations(0, false).assertRemovals(0, false);
	}
	
	@Test
	public void testRepositoryRepoTarget() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> mergerSC = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=dummy3)", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy3)(repoType=compare))", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy3");
		properties.put("repository.target", "(repo_id=delegate.mock)");
		Configuration c = createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		mergerSC.assertCreations(1, true).assertRemovals(0, false);
		repoSC.assertCreations(1, true).assertRemovals(0, false);
		
		deleteConfigurationAndRemoveFromCleanup(c);
		mergerSC.assertCreations(1, false).assertRemovals(1, true);
		repoSC.assertCreations(1, false).assertRemovals(1, true);
	}
	
	@Test
	public void testRepositoryWrongMergerTarget() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> mergerSC = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=dummy4)", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy4)(repoType=compare))", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy4");
		properties.put("repository.target", "(repo_id=delegate.mock)");
		properties.put("merger.target", "(bla=blub)");
		Configuration c = createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		mergerSC.assertCreations(1, true).assertRemovals(0, false);
		repoSC.assertCreations(0, true).assertRemovals(0, false);
		
		deleteConfigurationAndRemoveFromCleanup(c);
		mergerSC.assertCreations(1, false).assertRemovals(1, true);
		repoSC.assertCreations(0, false).assertRemovals(0, false);
	}
	
	@Test
	public void testRepositoryMergerTarget() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> mergerSC = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=hallo)", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy5)(repoType=compare))", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		
		Dictionary<String, Object> mprop = new Hashtable<String, Object>();
		mprop.put(ModelObjectMerger.MERGER_NAME, "hallo");
		mprop.put("mykey", "myvalue");
		Configuration mc = createConfigForCleanup(ModelObjectMerger.CONFIGURABLE_FACTORY_ID, "test",  "?", mprop);

		mergerSC.assertCreations(1, true).assertRemovals(0, false);
		repoSC.assertCreations(0, false).assertRemovals(0, false);

		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy5");
		properties.put("repository.target", "(repo_id=delegate.mock)");
		properties.put("merger.target", "(mykey=myvalue)");
		
		Configuration c = createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		mergerSC.assertCreations(1, false).assertRemovals(0, false);
		repoSC.assertCreations(1, true).assertRemovals(0, false);
		
		deleteConfigurationAndRemoveFromCleanup(mc);
		mergerSC.assertCreations(1, false).assertRemovals(1, true);
		repoSC.assertCreations(1, false).assertRemovals(1, true);
		
		deleteConfigurationAndRemoveFromCleanup(c);
		mergerSC.assertCreations(1, false).assertRemovals(1, false);
		repoSC.assertCreations(1, false).assertRemovals(1, false);
	}

	private void setupMockServices() {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("repo_id", "delegate.mock");
		registerServiceForCleanup(EMFRepository.class, new PrototypeServiceFactory<EMFRepository>() {

			@Override
			public EMFRepository getService(Bundle bundle, ServiceRegistration<EMFRepository> registration) {
				return repository;
			}

			@Override
			public void ungetService(Bundle bundle, ServiceRegistration<EMFRepository> registration, EMFRepository service) {
				repository.dispose();
			}
		}, properties);
		createTrackedChecker(EMFRepository.class).assertCreations(1, true);
	}
}
