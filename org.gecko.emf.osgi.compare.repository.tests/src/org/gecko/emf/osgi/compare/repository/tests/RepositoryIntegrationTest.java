/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.repository.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.gecko.emf.osgi.compare.repository.CompareConstants;
import org.gecko.emf.osgi.model.test.GenderType;
import org.gecko.emf.osgi.model.test.Person;
import org.gecko.emf.osgi.model.test.TestFactory;
import org.gecko.emf.osgi.model.test.TestPackage;
import org.gecko.emf.osgi.model.test.util.TestResourceFactoryImpl;
import org.gecko.emf.repository.EMFRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.PrototypeServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;


/**
 * <p>
 * Integration tests for the EMFCompareRepository
 * </p>
 * 
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class RepositoryIntegrationTest extends AbstractOSGiTest {
	
	@Mock
	private EMFRepository delegate;
	
	/**
	 * Creates a new instance.
	 * @param bundleContext
	 */
	public RepositoryIntegrationTest() {
		super(FrameworkUtil.getBundle(RepositoryIntegrationTest.class).getBundleContext());
	}

	/**
	 * Here you can put everything you want to be exectued before every test
	 */
	public void doBefore() {
		setupMockServices();
	}
	
	/**
	 * Here you can put everything you want to be exectued after every test
	 */
	public void doAfter() {
	}
	
	@Test
	public void testRepositorySimple() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> mergerSC = createTrackedChecker("(" + ModelObjectMerger.MERGER_NAME + "=dummy)", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy)(repoType=compare))", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy");
		properties.put("repository.target", "(repo_id=delegate.mock)");
		Configuration c = createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		mergerSC.assertCreations(1, true).assertRemovals(0, false);
		repoSC.assertCreations(1, true).assertRemovals(0, false);
		
		deleteConfigurationAndRemoveFromCleanup(c);
		mergerSC.assertCreations(1, false).assertRemovals(1, true);
		repoSC.assertCreations(1, false).assertRemovals(1, true);
	}
	
	@Test
	public void testRepositoryNewObjectNoId() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy)(repoType=compare))", true)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy");
		properties.put("repository.target", "(repo_id=delegate.mock)");
		createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		EMFRepository repository = (EMFRepository) repoSC
				.assertCreations(1, true)
				.assertRemovals(0, false)
				.getTrackedService();
		
		ResourceSet rs = new ResourceSetImpl();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("test", new TestResourceFactoryImpl());
		Mockito.when(delegate.createResourceSet()).thenReturn(rs);
		
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Mockito.when(delegate.getEObject(Mockito.any(EClass.class), Mockito.anyString(), Mockito.anyMap())).thenReturn(p1);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		repository.save(p2);
		
		assertNotEquals("Hoffmann", p1.getLastName());
		
		ArgumentCaptor<EObject> saveC = ArgumentCaptor.forClass(EObject.class);
		Mockito.verify(delegate, Mockito.times(1)).save(saveC.capture());
		Mockito.verify(delegate, Mockito.never()).getEObject(Mockito.any(EClass.class), Mockito.anyString(), Mockito.anyMap());
		
		Person ptos = (Person) saveC.getValue();
		assertEquals(p2, ptos);
		assertEquals("Hoffmann", ptos.getLastName());
		
	}
	
	@Test
	public void testRepositoryNewObjectWithId() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy)(repoType=compare))", false)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy");
		properties.put("repository.target", "(repo_id=delegate.mock)");
		createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		EMFRepository repository = (EMFRepository) repoSC
				.assertCreations(1, true)
				.assertRemovals(0, false)
				.getTrackedService();
		
		ResourceSet rs = new ResourceSetImpl();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("test", new TestResourceFactoryImpl());
		Mockito.when(delegate.createResourceSet()).thenReturn(rs);
		
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Mockito
		.when(delegate.getEObject(Mockito.any(EClass.class), Mockito.anyString(), Mockito.anyMap()))
		.thenReturn(null);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setId("mh");
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		repository.save(p2);
		
		assertNotEquals("Hoffmann", p1.getLastName());
		
		ArgumentCaptor<EObject> saveC = ArgumentCaptor.forClass(EObject.class);
		Mockito.verify(delegate, Mockito.times(1)).save(saveC.capture());
		Mockito.verify(delegate, Mockito.times(1)).getEObject(Mockito.any(EClass.class), Mockito.anyString(), Mockito.anyMap());
		
		Person ptos = (Person) saveC.getValue();
		assertEquals(p2, ptos);
		assertEquals("Hoffmann", ptos.getLastName());
		
	}
	
	@Test
	public void testRepositoryExistingObject() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy)(repoType=compare))", false)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy");
		properties.put("repository.target", "(repo_id=delegate.mock)");
		createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		EMFRepository repository = (EMFRepository) repoSC
				.assertCreations(1, true)
				.assertRemovals(0, false)
				.getTrackedService();
		
		ResourceSet rs = new ResourceSetImpl();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("test", new TestResourceFactoryImpl());
		Mockito.when(delegate.createResourceSet()).thenReturn(rs);
		
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setId("mh");
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Mockito
		.when(delegate.getEObject(Mockito.any(EClass.class), Mockito.anyString(), Mockito.anyMap()))
		.thenReturn(p1);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setId("mh");
		p2.setFirstName("Mark");
		p2.setLastName("Hoffmann");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		
		assertNotEquals("Hoffmann", p1.getLastName());
		repository.save(p2);
		
		
		ArgumentCaptor<EObject> saveC = ArgumentCaptor.forClass(EObject.class);
		Mockito.verify(delegate, Mockito.times(1)).save(saveC.capture());
		Mockito.verify(delegate, Mockito.times(1)).getEObject(Mockito.any(EClass.class), Mockito.anyString(), Mockito.anyMap());
		
		Person ptos = (Person) saveC.getValue();
		assertEquals(p1, ptos);
		assertEquals("Hoffmann", ptos.getLastName());
		
	}
	
	@Test
	public void testRepositoryNoChange() throws IOException, InvalidSyntaxException {
		ServiceChecker<Object> repoSC = createTrackedChecker("(&(" + EMFRepository.PROP_ID + "=dummy)(repoType=compare))", false)
				.assertCreations(0, false)
				.assertRemovals(0, false);
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("name", "dummy");
		properties.put("repository.target", "(repo_id=delegate.mock)");
		createConfigForCleanup(CompareConstants.COMPARE_CONFIGURATION_FACTORY_ID, "cde", "?", properties);
		EMFRepository repository = (EMFRepository) repoSC
				.assertCreations(1, true)
				.assertRemovals(0, false)
				.getTrackedService();
		
		ResourceSet rs = new ResourceSetImpl();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("test", new TestResourceFactoryImpl());
		Mockito.when(delegate.createResourceSet()).thenReturn(rs);
		
		Resource r1 = rs.createResource(URI.createURI("tmp1.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setId("mh");
		p1.setFirstName("Mark");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		Mockito
		.when(delegate.getEObject(Mockito.any(EClass.class), Mockito.anyString(), Mockito.anyMap()))
		.thenReturn(p1);
		
		Resource r2 = rs.createResource(URI.createURI("tmp2.test"));
		Person p2 = TestFactory.eINSTANCE.createPerson();
		p2.setId("mh");
		p2.setFirstName("Mark");
		p2.setGender(GenderType.MALE);
		r2.getContents().add(p2);
		
		assertNotEquals("Hoffmann", p1.getLastName());
		repository.save(p2);
		
		
		Mockito.verify(delegate, Mockito.never()).save(Mockito.any(EObject.class));
		Mockito.verify(delegate, Mockito.times(1)).getEObject(Mockito.any(EClass.class), Mockito.anyString(), Mockito.anyMap());
		
	}
	
	private void setupMockServices() {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("repo_id", "delegate.mock");
		registerServiceForCleanup(EMFRepository.class, new PrototypeServiceFactory<EMFRepository>() {

			@Override
			public EMFRepository getService(Bundle bundle, ServiceRegistration<EMFRepository> registration) {
				return delegate;
			}

			@Override
			public void ungetService(Bundle bundle, ServiceRegistration<EMFRepository> registration, EMFRepository service) {
				delegate.dispose();
			}
		}, properties);
		createTrackedChecker(EMFRepository.class).assertCreations(1, true);
	}
}
