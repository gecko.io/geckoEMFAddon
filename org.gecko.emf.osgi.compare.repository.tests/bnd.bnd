-include: ${workspace}/cnf/include/jdt.bnd
# Discover and run all test cases annotated with the @RunWith annotation
Test-Cases: ${classes;CONCRETE;ANNOTATED;org.junit.runner.RunWith}

# Needed for Mockito's mocking to work
-runsystempackages.objenesis: sun.misc,sun.reflect

-resolve.effective: active:="osgi.service"

# We need JUnit and Mockito to resolve the test cases at runtime. 
# Other runtime dependencies should be added as necessary
-runbundles.junit: \
	${junit},\
	${mockito},\
	${geckotest}

-enable-gecko-emf: true

-buildpath: \
	${junit},\
	${mockito},\
	${geckotest},\
	osgi.annotation;version=7.0,\
	osgi.core;version=7.0,\
	osgi.cmpn;version=7.0,\
	org.gecko.emf.osgi.compare.api;version=latest,\
	org.gecko.emf.osgi.compare.repository;version=latest,\
	org.gecko.emf.repository.api;version=9.2,\
	org.gecko.emf.osgi.test.model,\

Bundle-Version: 1.0.1.SNAPSHOT
Private-Package: org.gecko.emf.osgi.compare.repository.tests
-runfw: org.apache.felix.framework;version='[6.0.1,6.0.1]'
-runee: JavaSE-1.8
-runrequires: \
	bnd.identity;id='org.gecko.emf.osgi.compare.repository.tests',\
	bnd.identity;id='org.gecko.emf.osgi.compare.repository'
-runbundles: \
	org.apache.felix.configadmin;version='[1.9.16,1.9.17)',\
	org.apache.servicemix.bundles.junit;version='[4.12.0,4.12.1)',\
	org.mockito.mockito-core;version='[1.10.19,1.10.20)',\
	org.objenesis;version='[2.2.0,2.2.1)',\
	org.eclipse.emf.ecore.xmi;version='[2.16.0,2.16.1)',\
	slf4j.api;version='[1.7.25,1.7.26)',\
	org.osgi.util.function;version='[1.1.0,1.1.1)',\
	org.osgi.util.promise;version='[1.1.0,1.1.1)',\
	ch.qos.logback.core;version='[1.2.3,1.2.4)',\
	log4j.over.slf4j;version='[1.7.25,1.7.26)',\
	org.eclipse.core.contenttype;version='[3.7.400,3.7.401)',\
	org.eclipse.core.jobs;version='[3.10.500,3.10.501)',\
	org.eclipse.core.runtime;version='[3.16.100,3.16.101)',\
	org.eclipse.emf.compare;version='[3.5.3,3.5.4)',\
	org.eclipse.equinox.app;version='[1.4.300,1.4.301)',\
	org.eclipse.equinox.common;version='[3.10.500,3.10.501)',\
	org.eclipse.equinox.preferences;version='[3.7.500,3.7.501)',\
	org.eclipse.equinox.registry;version='[3.8.500,3.8.501)',\
	org.eclipse.equinox.supplement;version='[1.9.100,1.9.101)',\
	org.gecko.core.test;version='[2.0.4,2.0.5)',\
	ch.qos.logback.classic;version='[1.2.3,1.2.4)',\
	com.google.guava;version='[15.0.0,15.0.1)',\
	org.apache.felix.log;version='[1.2.4,1.2.5)',\
	org.apache.felix.scr;version='[2.1.24,2.1.25)',\
	org.eclipse.emf.common;version='[2.20.0,2.20.1)',\
	org.eclipse.emf.ecore;version='[2.23.0,2.23.1)',\
	org.gecko.emf.osgi.compare.api;version=snapshot,\
	org.gecko.emf.osgi.compare.component;version=snapshot,\
	org.gecko.emf.osgi.compare.repository;version=snapshot,\
	org.gecko.emf.osgi.compare.repository.tests;version=snapshot,\
	org.gecko.emf.osgi.component;version='[4.0.0,4.0.1)',\
	org.gecko.emf.osgi.test.model;version='[4.0.0,4.0.1)',\
	org.gecko.emf.repository.api;version='[9.2.2,9.2.3)'
-runrepos: \
	Workspace,\
	Maven Central,\
	Local,\
	Release,\
	Temp,\
	Templates,\
	DIM_Nexus,\
	DIM_Release,\
	GeckoEMF Workspace Extension