-include: ${workspace}/cnf/include/jdt.bnd
-enable-gecko-emf: true

# Discover and run all test cases annotated with the @RunWith annotation
Test-Cases: ${classes;CONCRETE;ANNOTATED;org.junit.runner.RunWith}

-includeresource: transforms/

# Build dependencies 
-buildpath: \
	${junit},\
	${mockito},\
	${geckotest},\
	osgi.annotation;version=7.0,\
	osgi.core;version=7.0,\
	osgi.cmpn;version=7.0,\
	org.eclipse.m2m.qvt.oml;version=latest,\
	org.gecko.qvt.osgi.api;version=latest,\
	org.gecko.qvt.osgi.component;version=latest,\
	org.gecko.core.pool,\
	org.gecko.emf.osgi.test.model

javac.source: 1.8
javac.target: 1.8

# We need JUnit and Mockito to resolve the test cases at runtime. 
# Other runtime dependencies should be added as necessary
-runbundles: \
	lpg.runtime.java;version='[2.0.17,2.0.18)',\
	org.apache.felix.configadmin;version='[1.9.16,1.9.17)',\
	org.gecko.eclipse.core.supplement;version='[1.0.0,1.0.1)',\
	org.apache.servicemix.bundles.junit;version='[4.12.0,4.12.1)',\
	org.mockito.mockito-core;version='[1.10.19,1.10.20)',\
	org.objenesis;version='[2.2.0,2.2.1)',\
	org.eclipse.emf.ecore.change;version='[2.14.0,2.14.1)',\
	org.eclipse.emf.ecore.xmi;version='[2.16.0,2.16.1)',\
	org.eclipse.m2m.qvt.oml.common;version='[3.9.0,3.9.1)',\
	org.eclipse.m2m.qvt.oml.cst.parser;version='[3.9.0,3.9.1)',\
	org.eclipse.m2m.qvt.oml.ecore.imperativeocl;version='[3.9.0,3.9.1)',\
	org.eclipse.m2m.qvt.oml.ocl;version='[3.9.0,3.9.1)',\
	org.eclipse.m2m.qvt.oml;version='[3.10.0,3.10.1)',\
	org.eclipse.m2m.qvt.oml.emf.util;version='[3.9.0,3.9.1)',\
	org.eclipse.ocl;version='[3.10.400,3.10.401)',\
	org.eclipse.ocl.common;version='[1.8.400,1.8.401)',\
	org.eclipse.ocl.ecore;version='[3.14.0,3.14.1)',\
	org.eclipse.core.contenttype;version='[3.7.400,3.7.401)',\
	org.eclipse.core.jobs;version='[3.10.500,3.10.501)',\
	org.eclipse.core.runtime;version='[3.16.100,3.16.101)',\
	org.eclipse.equinox.app;version='[1.4.300,1.4.301)',\
	org.eclipse.equinox.common;version='[3.10.500,3.10.501)',\
	org.eclipse.equinox.preferences;version='[3.7.500,3.7.501)',\
	org.eclipse.equinox.registry;version='[3.8.500,3.8.501)',\
	org.eclipse.equinox.supplement;version='[1.9.100,1.9.101)',\
	org.osgi.util.function;version='[1.1.0,1.1.1)',\
	org.osgi.util.promise;version='[1.1.0,1.1.1)',\
	org.gecko.qvt.osgi.api;version='[4.1.0,4.1.1)',\
	org.gecko.core.pool;version='[1.1.0,1.1.1)',\
	org.gecko.core.test;version='[2.0.4,2.0.5)',\
	org.apache.felix.scr;version='[2.1.24,2.1.25)',\
	org.eclipse.emf.common;version='[2.20.0,2.20.1)',\
	org.eclipse.emf.ecore;version='[2.23.0,2.23.1)',\
	org.gecko.emf.osgi.component;version='[4.0.0,4.0.1)',\
	org.gecko.emf.osgi.test.model;version='[4.0.0,4.0.1)',\
	org.gecko.qvt.osgi.component;version=snapshot,\
	org.gecko.qvt.osgi.tests;version=snapshot,\
	org.apache.felix.log;version='[1.2.4,1.2.5)'
Bundle-Version: 1.0.7.SNAPSHOT
Bundle-Copyright: Data In Motion Consulting
Bundle-License: Eclipse Public License v1.0
Bundle-Name: M2M QVT Integration Tests
Bundle-Description: Model to model QVT transformation integration tests
Bundle-Vendor: Data In Motion Consulting GmbH
Private-Package: \
	org.gecko.qvt.osgi.tests,\
	org.gecko.qvt.osgi.tests.bbox

# Needed for Mockito's mocking to work
-runsystempackages.objenesis: sun.misc,sun.reflect

-resolve.effective: active;skip:="osgi.service"

# Use Felix by default
-runfw: org.apache.felix.framework;version='[6.0.1,6.0.1]'
-runvm: -ea
-runrequires: \
	bnd.identity;id='org.gecko.eclipse.core.supplement',\
	bnd.identity;id='org.gecko.emf.osgi.test.model',\
	bnd.identity;id='org.gecko.qvt.osgi.tests',\
	bnd.identity;id='org.apache.felix.scr'
-runee: JavaSE-11
-runblacklist: bnd.identity;version='3.13.100';id='org.eclipse.osgi'
