/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.EPackageConfigurator;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.emf.osgi.model.test.GenderType;
import org.gecko.emf.osgi.model.test.Person;
import org.gecko.emf.osgi.model.test.TestFactory;
import org.gecko.emf.osgi.model.test.TestPackage;
import org.gecko.qvt.osgi.api.ModelTransformationConstants;
import org.gecko.qvt.osgi.api.ModelTransformationFactory;
import org.gecko.qvt.osgi.api.ModelTransformator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;

/**
 * Test QVT Mapping
 * @author mark
 * @since 20.10.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfigurableTransformatorModelBindingIntegrationTest extends AbstractOSGiTest {

	public ConfigurableTransformatorModelBindingIntegrationTest() {
		super(FrameworkUtil.getBundle(ConfigurableTransformatorModelBindingIntegrationTest.class).getBundleContext());
	}
	
	@Override
	public void doBefore() {
		Dictionary<String, Object> props = new Hashtable<String, Object>();
		props.put("emf.model.name", "test");
		registerServiceForCleanup(EPackageConfigurator.class, new TestConfigurator(), props);
	}
	
	@Override
	public void doAfter() {
		// TODO Auto-generated method stub
		
	}

	@Test
	public void testWithModelBinding() throws InterruptedException, URISyntaxException, IOException, InvalidSyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		EPackage ePackage = rs.getPackageRegistry().getEPackage(TestPackage.eNS_URI);
		assertNotNull(ePackage);
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		ServiceChecker<Object> trafoSC = createTrackedChecker("(" + ModelTransformationConstants.TRANSFORMATOR_NAME + "=test-trafo)", true);
		trafoSC.assertCreations(0, false).assertRemovals(0, false);
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put(ModelTransformationConstants.TRANSFORMATOR_NAME, "test-trafo");
		String path = getBundleContext().getBundle().getSymbolicName();
		path += ":"  + getBundleContext().getBundle().getVersion().toString();
		path += "/PersonTransformation.qvto";
		properties.put(ModelTransformationConstants.TEMPLATE_PATH, path);
		properties.put(ModelTransformationConstants.MODEL_TARGET, "(emf.model.name=test)");
		Configuration configuration = createConfigForCleanup(ModelTransformationConstants.CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, "test-trafo", "?", properties);
		
		trafoSC.assertCreations(1, true).assertRemovals(0, false);
		ModelTransformator transformator = (ModelTransformator) trafoSC.getTrackedService();
		assertNotNull(transformator);
		
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		
		deleteConfigurationAndRemoveFromCleanup(configuration);
		trafoSC.assertCreations(1, false).assertRemovals(1, true);
	}
	
	@Test
	public void testWrongModelBinding() throws InterruptedException, URISyntaxException, IOException, InvalidSyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		EPackage ePackage = rs.getPackageRegistry().getEPackage(TestPackage.eNS_URI);
		assertNotNull(ePackage);
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		ServiceChecker<Object> trafoSC = createTrackedChecker("(" + ModelTransformationConstants.TRANSFORMATOR_NAME + "=test-trafo)", true);
		trafoSC.assertCreations(0, false);
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put(ModelTransformationConstants.TRANSFORMATOR_NAME, "test-trafo");
		String path = getBundleContext().getBundle().getSymbolicName();
		path += ":"  + getBundleContext().getBundle().getVersion().toString();
		path += "/PersonTransformation.qvto";
		properties.put(ModelTransformationConstants.TEMPLATE_PATH, path);
		properties.put(ModelTransformationConstants.MODEL_TARGET, "(emf.model.name=test-somthing)");
		createConfigForCleanup(ModelTransformationConstants.CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, "test-trafo", "?", properties);
		trafoSC.assertCreations(0, true);
		
	}
	
}