/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Basic implementation for the test setup 
 * @author Mark Hoffmann
 * @since 18.10.2017
 */
public class BasicExampleTest {
	
	private BundleContext context;

	public void setup(BundleContext context) {
		setBundleContext(context);
	}
	
	public void teardown() {
	}
	
	public void setBundleContext(BundleContext context) {
		this.context = context;
	}
	
	/**
	 * Creates a configuration with the configuration admin
	 * @param configId the configuration id
	 * @return the configuration
	 * @throws Exception
	 */
	protected Configuration getConfiguration(String configId) throws Exception {
		assertNotNull(context);
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);

		// create MQTT client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(configId, "?");
		assertNotNull(clientConfig);

		return clientConfig;
	}
	
	/**
	 * Gets a service by class and waits for it 
	 * @param clazz
	 * @param timeout
	 * @return
	 * @throws InterruptedException
	 */
	protected <T> T getService(Class<T> clazz, long timeout) throws InterruptedException {
		assertNotNull(context);
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, clazz, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}
	
	/**
	 * Gets a service by filter and waits for it 
	 * @param filter
	 * @param timeout
	 * @return
	 * @throws InterruptedException
	 */
	protected <T> T getService(Filter filter, long timeout) throws InterruptedException {
		assertNotNull(context);
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, filter, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}

}
