/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.TransformationExecutor.BlackboxRegistry;
import org.gecko.core.pool.Pool;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.emf.osgi.model.test.Address;
import org.gecko.emf.osgi.model.test.GenderType;
import org.gecko.emf.osgi.model.test.Person;
import org.gecko.emf.osgi.model.test.TestFactory;
import org.gecko.emf.osgi.model.test.TestPackage;
import org.gecko.qvt.osgi.api.ModelTransformationConstants;
import org.gecko.qvt.osgi.api.ModelTransformationFactory;
import org.gecko.qvt.osgi.api.ModelTransformator;
import org.gecko.qvt.osgi.tests.bbox.BlackboxTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * Test QVT Mapping
 * @author mark
 * @since 20.10.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class QVTTransformatorIntegrationTest extends AbstractOSGiTest {

	public QVTTransformatorIntegrationTest() {
		super(FrameworkUtil.getBundle(QVTTransformatorIntegrationTest.class).getBundleContext());
	}
	
	@Override
	public void doBefore() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void doAfter() {
		// TODO Auto-generated method stub
		
	}

	@Test
	public void testExample() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		System.out.println("bsn: " + b.getSymbolicName());
		URL resource = getBundleContext().getBundle().getResource("PersonTransformation.qvto");
		System.out.println("resource " + resource);
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		ModelTransformator transformator = mtf.createModelTransformator(rs, mapping);
		assertNotNull(transformator);
		
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
	}

	@Test
	public void testWithPool() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		System.out.println("bsn: " + b.getSymbolicName());
		URL resource = getBundleContext().getBundle().getResource("PersonTransformation.qvto");
		System.out.println("resource " + resource);
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		Pool<ModelTransformator> transformatorPool = mtf.createNewModelTransformatorPool(rs, mapping, 5, 100);
		assertNotNull(transformatorPool);
		
		ModelTransformator transformator = transformatorPool.poll();
		
		EObject result = transformator.startTransformation(p1);
		transformatorPool.release(transformator);
		transformatorPool.dispose();
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		
	}
	
	@Test
	public void testExampleWithDeps() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		Address address = TestFactory.eINSTANCE.createAddress();
		address.setCity("Gera");
		address.setStreet("Kurt-Keicher");
		p1.setAddress(address);
		
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		System.out.println("bsn: " + b.getSymbolicName());
		URL resource = getBundleContext().getBundle().getResource("PersonTransformationWithDeps.qvto");
		System.out.println("resource " + resource);
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		ModelTransformator transformator = mtf.createModelTransformator(rs, mapping);
		assertNotNull(transformator);
		
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		assertNotNull(resultPerson.getAddress());
		Address resultAddress = resultPerson.getAddress();
		assertEquals("SesamKurt-Keicher", resultAddress.getStreet());
		assertEquals("MyBeautifulGera", resultAddress.getCity());
	}
	
	@Test
	public void testExampleWithDepsLibrary() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		Address address = TestFactory.eINSTANCE.createAddress();
		address.setCity("Gera");
		address.setStreet("Kurt-Keicher");
		p1.setAddress(address);
		
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		System.out.println("bsn: " + b.getSymbolicName());
		URL resource = getBundleContext().getBundle().getResource("PersonTransformationWithDepsLib.qvto");
		System.out.println("resource " + resource);
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		ModelTransformator transformator = mtf.createModelTransformator(rs, mapping);
		assertNotNull(transformator);
		
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		assertNotNull(resultPerson.getAddress());
		Address resultAddress = resultPerson.getAddress();
		assertEquals("SesamKurt-Keicher", resultAddress.getStreet());
		assertEquals("MyBeautifulGera", resultAddress.getCity());
	}
	
	@Test
	public void testExampleWithBlackbox01() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		Address address = TestFactory.eINSTANCE.createAddress();
		address.setCity("Gera");
		address.setStreet("Kurt-Keicher");
		p1.setAddress(address);
		
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		System.out.println("bsn: " + b.getSymbolicName());
		URL resource = getBundleContext().getBundle().getResource("PersonTransformationWithBlackbox.qvto");
		System.out.println("resource " + resource);
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		BlackboxRegistry.INSTANCE.registerModule(BlackboxTest.class);
		ModelTransformator transformator = mtf.createModelTransformator(rs, mapping);
		assertNotNull(transformator);
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		assertNotNull(resultPerson.getAddress());
		Address resultAddress = resultPerson.getAddress();
		assertEquals("Kurt-KeicherCopy", resultAddress.getStreet());
		assertEquals("GeraCopy", resultAddress.getCity());
	}
	
	@Test
	public void testExampleWithBlackbox02() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		Address address = TestFactory.eINSTANCE.createAddress();
		address.setCity("Gera");
		address.setStreet("Kurt-Keicher");
		p1.setAddress(address);
		
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		URL resource = b.getResource("PersonTransformationWithBlackbox.qvto");
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		TransformationExecutor.BlackboxRegistry.INSTANCE.registerModule(BlackboxTest.class);
		ModelTransformator transformator = mtf.createModelTransformator(rs, mapping);
		assertNotNull(transformator);
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		assertNotNull(resultPerson.getAddress());
		Address resultAddress = resultPerson.getAddress();
		assertEquals("Kurt-KeicherCopy", resultAddress.getStreet());
		assertEquals("GeraCopy", resultAddress.getCity());
	}
	
	@Test
	public void testExampleWithBlackboxAlternativeName01() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		Address address = TestFactory.eINSTANCE.createAddress();
		address.setCity("Gera");
		address.setStreet("Kurt-Keicher");
		p1.setAddress(address);
		
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		URL resource = b.getResource("PersonTransformationWithBlackboxAltName.qvto");
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		TransformationExecutor.BlackboxRegistry.INSTANCE.registerModule(BlackboxTest.class, "org.gecko.MyBB", "MyBBTest");
		ModelTransformator transformator = mtf.createModelTransformator(rs, mapping);
		assertNotNull(transformator);
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		assertNotNull(resultPerson.getAddress());
		Address resultAddress = resultPerson.getAddress();
		assertEquals("Kurt-KeicherCopy", resultAddress.getStreet());
		assertEquals("GeraCopy", resultAddress.getCity());
	}
	
	@Test
	public void testExampleWithBlackboxAlternativeName02() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		Address address = TestFactory.eINSTANCE.createAddress();
		address.setCity("Gera");
		address.setStreet("Kurt-Keicher");
		p1.setAddress(address);
		
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		URL resource = b.getResource("PersonTransformationWithBlackboxAltName.qvto");
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		TransformationExecutor.BlackboxRegistry.INSTANCE.registerModule(BlackboxTest.class, "org.gecko.MyBB", "MyBlackboxTest");
		ModelTransformator transformator = mtf.createModelTransformator(rs, mapping);
		assertNotNull(transformator);
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		assertNotNull(resultPerson.getAddress());
		Address resultAddress = resultPerson.getAddress();
		assertEquals("Kurt-KeicherCopy", resultAddress.getStreet());
		assertEquals("GeraCopy", resultAddress.getCity());
	}
	
	@Test
	public void testExampleWithBlackboxService01() throws InterruptedException, URISyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		Address address = TestFactory.eINSTANCE.createAddress();
		address.setCity("Gera");
		address.setStreet("Kurt-Keicher");
		p1.setAddress(address);
		
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Bundle b = getBundleContext().getBundle();
		URL resource = b.getResource("PersonTransformationWithBlackboxService.qvto");
		java.net.URI uri = resource.toURI();
		URI mapping = URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
		
		// register transformator manually, because it is not registered as component
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		ModelTransformator transformator;
		try {
			transformator = mtf.createModelTransformator(rs, mapping);
			fail("Not expected to reach this line");
		} catch (IllegalStateException ise) {
			assertTrue(ise.getMessage().startsWith("Source:"));
		} catch (Exception e) {
			fail("Not expected to get this exception type");
		}
		
		ServiceChecker<BlackboxTest> bbSC = createTrackedChecker(BlackboxTest.class).assertCreations(0, false).assertRemovals(0, false);;

		BlackboxTest bbt = new BlackboxTest();
		registerServiceForCleanup(BlackboxTest.class, bbt, new Hashtable<String, Object>());
		
		BlackboxTest bbService =  bbSC.assertCreations(1, true).assertRemovals(0, false).getTrackedService();
		assertNotNull(bbService);
		try {
			transformator = mtf.createModelTransformator(rs, mapping);
			fail("Not expected to reach this line");
		} catch (IllegalStateException ise) {
			assertTrue(ise.getMessage().startsWith("Source:"));
		} catch (Exception e) {
			fail("Not expected to get this exception type");
		}
		unregisterService(bbt);
		
		bbSC.assertCreations(1, false).assertRemovals(1, true);
		
		Dictionary<String, Object> properties = new Hashtable<>();
		properties.put(ModelTransformationConstants.QVT_BLACKBOX, "true");
		properties.put(ModelTransformationConstants.BLACKBOX_MODULENAME, "MyserviceBB");
		properties.put(ModelTransformationConstants.BLACKBOX_QUALIFIED_UNIT_NAME, "org.gecko.service.MyBB");
		
		registerServiceForCleanup(BlackboxTest.class, bbt, properties);
		
		bbSC.assertCreations(2, true).assertRemovals(1, false);
		
		transformator = mtf.createNewModelTransformator(rs, mapping);
		
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		assertNotNull(resultPerson.getAddress());
		Address resultAddress = resultPerson.getAddress();
		assertEquals("Kurt-KeicherCopy", resultAddress.getStreet());
		assertEquals("GeraCopy", resultAddress.getCity());
		
		unregisterService(bbt);
		bbSC.assertCreations(2, false).assertRemovals(2, true);
		
	}
	
}