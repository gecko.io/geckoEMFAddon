package org.gecko.qvt.osgi.tests;

import org.eclipse.emf.ecore.EPackage.Registry;
import org.gecko.emf.osgi.EPackageConfigurator;
import org.gecko.emf.osgi.model.test.TestPackage;

public class TestConfigurator implements EPackageConfigurator {

	@Override
	public void configureEPackage(Registry registry) {
		registry.put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
	}

	@Override
	public void unconfigureEPackage(Registry registry) {
		registry.remove(TestPackage.eNS_URI);
	}

}
