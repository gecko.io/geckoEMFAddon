package org.gecko.qvt.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.core.pool.ConfigurablePoolComponent;
import org.gecko.core.pool.Pool;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.emf.osgi.model.test.GenderType;
import org.gecko.emf.osgi.model.test.Person;
import org.gecko.emf.osgi.model.test.TestFactory;
import org.gecko.emf.osgi.model.test.TestPackage;
import org.gecko.qvt.osgi.api.ModelTransformationConstants;
import org.gecko.qvt.osgi.api.ModelTransformationFactory;
import org.gecko.qvt.osgi.api.ModelTransformator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;

/**
 * Checks the implmentation of the ConfigurableModelTransformatorPool component
 * 
 * @author ilenia
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfigurableModelTransformatorPoolIntegrationTest extends AbstractOSGiTest {
	
	public ConfigurableModelTransformatorPoolIntegrationTest() {
		super(FrameworkUtil.getBundle(ConfigurableModelTransformatorPoolIntegrationTest.class).getBundleContext());
	}

	@Override
	public void doBefore() {
		for (Bundle b : getBundleContext().getBundles()) {
			if ("org.eclipse.emf.common".equals(b.getSymbolicName())) {
				try {
					b.start();
				} catch (BundleException e) {
					e.printStackTrace();
				}
			}
		}		
	}

	@Override
	public void doAfter() {
		
	}
	
	/**
	 * If the configuration for a ConfigurableModelTransformatorPool does not contain a property
	 * "pool.componentName", then the component will not be activated.
	 * 
	 * @throws IOException
	 * @throws InvalidSyntaxException
	 */
	@Test 
	public void testServiceSetup_ConfigNotOK() throws IOException, InvalidSyntaxException {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("pool.asService", true);
		createConfigForCleanup("ConfigurableModelTransformatorPool", "test", "?", properties);
		
		ServiceChecker<Object> trafoSC = createTrackedChecker("(service.factoryPid=ConfigurableModelTransformatorPool)", false);
		trafoSC.start();
		trafoSC.setCreateTimeout(5000);	
		trafoSC.assertCreations(0, true);	
	}
	
	/**
	 * If, after proper registration of the ConfigurableModelTransformatorPool, a ModelTransformator 
	 * is registered (for instance, through a PrototypeConfigurableTransformationService configuration,
	 * then a Pool<ModelTransformator> is created, and, if the pool.asService property is set to
	 * <code>true<code> the pool will be registered also as service
	 * 
	 * @throws IOException
	 * @throws InvalidSyntaxException
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testServiceSetup_RegisterAsService() throws IOException, InvalidSyntaxException, InterruptedException {		
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("pool.componentName", "ComponentTest");		
		properties.put("pool.asService", true);
		createConfigForCleanup("ConfigurableModelTransformatorPool", "test", "?", properties);
		
		ServiceChecker<Object> trafoSC = createTrackedChecker("(service.factoryPid=ConfigurableModelTransformatorPool)", false);
		trafoSC.start();
		trafoSC.setCreateTimeout(5000);	
		trafoSC.assertCreations(1, true);	
		
		ConfigurablePoolComponent<ModelTransformator> poolComponent = (ConfigurablePoolComponent<ModelTransformator>) trafoSC.getTrackedService();
		assertNotNull(poolComponent);
		
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
//		Create Model Transformator
		Dictionary<String, Object> poolProperties = new Hashtable<String, Object>();
		poolProperties.put("pool.name", "PoolTest");		
		poolProperties.put("pool.asService", true);
		poolProperties.put(ModelTransformationConstants.TRANSFORMATOR_NAME, "test-trafo2");
		String path = getBundleContext().getBundle().getSymbolicName();
		path += ":"  + getBundleContext().getBundle().getVersion().toString();
		path += "/PersonTransformation.qvto";
		poolProperties.put(ModelTransformationConstants.TEMPLATE_PATH, path);
		Configuration config = createConfigForCleanup(ModelTransformationConstants.PROTOTYPE_CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, "test1", "?", poolProperties);
		
		ServiceChecker<Object> modelTransformatorSC = createTrackedChecker("(service.factoryPid=PrototypeConfigurableTransformationService)", false);
		modelTransformatorSC.start();
		modelTransformatorSC.setCreateTimeout(5000);
		modelTransformatorSC.assertCreations(1, true);	
		
		ServiceChecker<Object> poolServiceSC = createTrackedChecker("(pool.combinedId=*)", false);
		poolServiceSC.start();
		poolServiceSC.setCreateTimeout(5000);			
		poolServiceSC.assertCreations(1, true);	
		
		ServiceReference<Object> poolServiceRef = poolServiceSC.getTrackedServiceReference();
		assertNotNull(poolServiceRef);
		
		Pool<ModelTransformator> poolService = (Pool<ModelTransformator>) poolServiceSC.getTrackedService();
		assertNotNull(poolService);
		
		Dictionary<String, Object> serviceProp = poolServiceRef.getProperties();
		assertNotNull(serviceProp.get("pool.combinedId"));
		assertNotNull(serviceProp.get("pool.name"));
		assertNotNull(serviceProp.get("pool.componentName"));
		assertNotNull(serviceProp.get("pool.size"));
		assertNotNull(serviceProp.get("pool.timeout"));
		assertNotNull(serviceProp.get("pool.asService"));
		
		assertEquals("ComponentTest-PoolTest", serviceProp.get("pool.combinedId"));
		assertEquals("ComponentTest", serviceProp.get("pool.componentName"));
		assertEquals("PoolTest", serviceProp.get("pool.name"));
		assertEquals(5, serviceProp.get("pool.size"));
		assertEquals(100, serviceProp.get("pool.timeout"));
		assertEquals(true, serviceProp.get("pool.asService"));
		
		assertEquals(1, poolComponent.getPoolMap().size());
		
		deleteConfigurationAndRemoveFromCleanup(config);
		poolServiceSC.assertRemovals(1, true);
		Thread.sleep(2000);
		assertEquals(0, poolComponent.getPoolMap().size());
		
	}
	
	/**
	 * If, after proper registration of the ConfigurableModelTransformatorPool, a ModelTransformator 
	 * is registered (for instance, through a PrototypeConfigurableTransformationService configuration,
	 * then a Pool<ModelTransformator> is created, and, if the pool.asService property is set to
	 * <code>false<code> the pool will only be stored in the ConfigurableModelTransformatorPool map 
	 * and can be used by retrieving the map
	 * 
	 * @throws IOException
	 * @throws InvalidSyntaxException
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testServiceSetup_RegisterNOTAsService() throws IOException, InvalidSyntaxException, InterruptedException {		
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("pool.componentName", "ComponentTest");		
		properties.put("pool.asService", true);
		createConfigForCleanup("ConfigurableModelTransformatorPool", "test", "?", properties);
		
		ServiceChecker<Object> trafoSC = createTrackedChecker("(service.factoryPid=ConfigurableModelTransformatorPool)", false);
		trafoSC.start();
		trafoSC.setCreateTimeout(5000);	
		trafoSC.assertCreations(1, true);	
		
		ConfigurablePoolComponent<ModelTransformator> poolComponent = (ConfigurablePoolComponent<ModelTransformator>) trafoSC.getTrackedService();
		assertNotNull(poolComponent);
		
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
//		Create pool with pool.asService property false --> no service should be registered
		Dictionary<String, Object> poolProperties2 = new Hashtable<String, Object>();
		poolProperties2.put("pool.name", "PoolTest2");		
		poolProperties2.put("pool.asService", false);
		poolProperties2.put(ModelTransformationConstants.TRANSFORMATOR_NAME, "test");
		String path = getBundleContext().getBundle().getSymbolicName();
		path += ":"  + getBundleContext().getBundle().getVersion().toString();
		path += "/PersonTransformation.qvto";
		poolProperties2.put(ModelTransformationConstants.TEMPLATE_PATH, path);
		Configuration config = createConfigForCleanup(ModelTransformationConstants.PROTOTYPE_CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, "test2", "?", poolProperties2);
		
		ServiceChecker<Object> modelTransformatorSC = createTrackedChecker("(service.factoryPid=PrototypeConfigurableTransformationService)", false);
		modelTransformatorSC.start();
		modelTransformatorSC.setCreateTimeout(5000);
		modelTransformatorSC.assertCreations(1, true);	
		
		ServiceReference<Object> modelTransformatorRef = modelTransformatorSC.getTrackedServiceReference();
		assertNotNull(modelTransformatorRef);
		
		ServiceChecker<Object> poolSC2 = createTrackedChecker("(pool.combinedId=ComponentTest-PoolTest2)", false);
		poolSC2.start();
		poolSC2.assertCreations(0, true);	
		
//		the pool should be still stored in the map, even if it is not a service
		assertEquals(1, poolComponent.getPoolMap().size());
		
		deleteConfigurationAndRemoveFromCleanup(config);
		poolSC2.assertRemovals(0, true);
		Thread.sleep(2000);
		assertEquals(0, poolComponent.getPoolMap().size());
	}
	
	
}
