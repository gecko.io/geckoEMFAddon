package org.gecko.qvt.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.emf.osgi.model.test.GenderType;
import org.gecko.emf.osgi.model.test.Person;
import org.gecko.emf.osgi.model.test.TestFactory;
import org.gecko.emf.osgi.model.test.TestPackage;
import org.gecko.qvt.osgi.api.ModelTransformationConstants;
import org.gecko.qvt.osgi.api.ModelTransformationFactory;
import org.gecko.qvt.osgi.api.ModelTransformator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;

@RunWith(MockitoJUnitRunner.class)
public class PrototypeConfigurableTransformatorIntegrationTest extends AbstractOSGiTest {

	public PrototypeConfigurableTransformatorIntegrationTest() {
		super(FrameworkUtil.getBundle(PrototypeConfigurableTransformatorIntegrationTest.class).getBundleContext());
	}

	@Override
	public void doBefore() {
		for (Bundle b : getBundleContext().getBundles()) {
			if ("org.eclipse.emf.common".equals(b.getSymbolicName())) {
				try {
					b.start();
				} catch (BundleException e) {
					e.printStackTrace();
				}
			}
		}		
	}

	@Override
	public void doAfter() {
		
	}
	
	/**
	 * Creates two PrototypeConfigurableTransformationService config and checks that two Services come up
	 * @throws InterruptedException
	 * @throws URISyntaxException
	 * @throws IOException
	 * @throws InvalidSyntaxException
	 */
	@Test
	public void testPrototypeConfigurableTransformator_Success() throws InterruptedException, URISyntaxException, IOException, InvalidSyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put(ModelTransformationConstants.TRANSFORMATOR_NAME, "test-trafo2");
		String path = getBundleContext().getBundle().getSymbolicName();
		path += ":"  + getBundleContext().getBundle().getVersion().toString();
		path += "/PersonTransformation.qvto";
		properties.put(ModelTransformationConstants.TEMPLATE_PATH, path);
		Configuration configuration = createConfigForCleanup(ModelTransformationConstants.PROTOTYPE_CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, "test-trafo2", "?", properties);
		
		ServiceChecker<Object> trafoSC = createTrackedChecker("(" + ModelTransformationConstants.TRANSFORMATOR_NAME + "=test-trafo2)", true);
		trafoSC.start();
		trafoSC.setCreateTimeout(5000);		
		trafoSC.assertCreations(1, true);		
		
		ModelTransformator transformator = (ModelTransformator) trafoSC.getTrackedService();
		EObject result = transformator.startTransformation(p1);
		assertNotNull(result);
		assertTrue(result instanceof Person);
		Person resultPerson = (Person) result;
		assertEquals(GenderType.FEMALE, resultPerson.getGender());
		assertEquals("Markin", resultPerson.getFirstName());
		assertEquals("Hoffmannin", resultPerson.getLastName());
		
		Dictionary<String, Object> properties2 = new Hashtable<String, Object>();
		properties2.put(ModelTransformationConstants.TRANSFORMATOR_NAME, "test-trafo2");
		properties2.put("id", "secondone");
		String path2 = getBundleContext().getBundle().getSymbolicName();
		path2 += ":"  + getBundleContext().getBundle().getVersion().toString();
		path2 += "/PersonTransformation.qvto";
		properties2.put(ModelTransformationConstants.TEMPLATE_PATH, path2);
		Configuration configuration2 = createConfigForCleanup(ModelTransformationConstants.PROTOTYPE_CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, "test-trafo3", "?", properties2);

		trafoSC.assertCreations(2, true);

		deleteConfigurationAndRemoveFromCleanup(configuration);
		deleteConfigurationAndRemoveFromCleanup(configuration2);
		trafoSC.assertCreations(2, false).assertRemovals(2, true);		
	}
	
	@Test
	public void testConfigurableTransformatorMissingPath() throws InterruptedException, URISyntaxException, IOException, InvalidSyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();		
		assertNotNull(mtf);
		
		ServiceChecker<Object> trafoSC = createTrackedChecker("(" + ModelTransformationConstants.TRANSFORMATOR_NAME + "=test-trafo3)", true);
		trafoSC.assertCreations(0, false).assertRemovals(0, false);
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put(ModelTransformationConstants.TRANSFORMATOR_NAME, "test-trafo3");
		Configuration configuration = createConfigForCleanup(ModelTransformationConstants.PROTOTYPE_CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, "test-trafo3", "?", properties);
		
		trafoSC.assertCreations(0, true).assertRemovals(0, false);
		
		deleteConfigurationAndRemoveFromCleanup(configuration);
		
		trafoSC.assertCreations(0, false).assertRemovals(0, true);
	}
	
	@Test
	public void testConfigurableTransformatorMissingName() throws InterruptedException, URISyntaxException, IOException, InvalidSyntaxException {
		ResourceSetFactory rsf = createStaticTrackedChecker(ResourceSetFactory.class).run().getTrackedService();
		assertNotNull(rsf);
		ResourceSet rs = rsf.createResourceSet();
		rs.getPackageRegistry().put(TestPackage.eNS_URI, TestPackage.eINSTANCE);
		Resource r1 = rs.createResource(URI.createURI("tmp.test"));
		Person p1 = TestFactory.eINSTANCE.createPerson();
		p1.setFirstName("Mark");
		p1.setLastName("Hoffmann");
		p1.setGender(GenderType.MALE);
		r1.getContents().add(p1);
		
		ModelTransformationFactory mtf = createStaticTrackedChecker(ModelTransformationFactory.class).run().getTrackedService();
		assertNotNull(mtf);
		
		ServiceChecker<Object> trafoSC = createTrackedChecker("(" + ModelTransformationConstants.TRANSFORMATOR_NAME + "=test-trafo4)", true).assertCreations(0, false).assertRemovals(0, false);
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		String path = getBundleContext().getBundle().getSymbolicName();
		path += ":"  + getBundleContext().getBundle().getVersion().toString();
		path += "/PersonTransformation.qvto";
		properties.put(ModelTransformationConstants.TEMPLATE_PATH, path);
		Configuration configuration = createConfigForCleanup(ModelTransformationConstants.PROTOTYPE_CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, "test-trafo4", "?", properties);
		
		trafoSC.assertCreations(0, true).assertRemovals(0, false);
		
		deleteConfigurationAndRemoveFromCleanup(configuration);
		
		trafoSC.assertCreations(0, false).assertRemovals(0, true);
	}

}
