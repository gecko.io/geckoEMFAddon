/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.api;

import java.util.List;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.osgi.annotation.versioning.ConsumerType;

/**
 * Interface to compare and merge model objects
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
@ConsumerType
public interface ModelObjectMerger {

	public static final String FACTORY_ID = "ModelObjectMergerFactory";
	public static final String CONFIGURABLE_FACTORY_ID = "ConfigurableModelObjectMerger";
	public static final String VISITOR_REFERENCE = "diffVisitor";
	public static final String COMPARISON_REFERENCE = "comparisonFactory";
	public static final String NAME = "name";
	public static final String MERGER_NAME = "merger";
	/**
	 * Compares the two resources and merges them into the right, the old object
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @return the {@link ModelCompareResultType} as result identifier
	 */
	public ModelCompareResultType compareAndMerge(Resource resourceNew, Resource resourceOld);

	/**
	 * Compares the two resources and merges them into the right, the old object
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @param ignoreFeatures a list of {@link EStructuralFeature} to be ignored during compare
	 * @return the {@link ModelCompareResultType} as result identifier
	 */
	public ModelCompareResultType compareAndMerge(Resource resourceNew, Resource resourceOld, List<EStructuralFeature> ignoreFeatures);

	/**
	 * Compares the two resources and returns the changes 
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @return the {@link ModelCompareResultType} as result identifier
	 */
	public ModelCompareResultType compare(Resource resourceNew, Resource resourceOld);

	/**
	 * Compares the two resources and returns the changes 
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @param ignoreFeatures a list of {@link EStructuralFeature} to be ignored during compare
	 * @return the {@link ModelCompareResultType} as result identifier
	 */
	public ModelCompareResultType compare(Resource resourceNew, Resource resourceOld, List<EStructuralFeature> ignoreFeatures);

	/**
	 * Compares the two resources and returns the changes 
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @param ignoreFeatures a list of {@link EStructuralFeature} to be ignored during compare, can be <code>null</code>
	 * @return the comparison containing all differences
	 */
	public Comparison compareRaw(Resource resourceNew, Resource resourceOld, List<EStructuralFeature> ignoreFeatures);

	/**
	 * Merges differences
	 * @param differences the compare differences to merge
	 * @return <code>true</code>, if merge was successful, otherwise <code>false</code>
	 */
	public boolean merge(List<Diff> differences);

	/**
	 * Fires finished visiting call to all visitors.
	 * @param resource the changed, merged resource 
	 */
	public void fireVisitorChanges(Resource resource);

}
