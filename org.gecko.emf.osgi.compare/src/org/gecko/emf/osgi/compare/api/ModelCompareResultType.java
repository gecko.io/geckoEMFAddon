/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.api;

/**
 * Result types for object comparer and merger
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
public enum ModelCompareResultType {
	
	// compare was successfull but no difference was found
	COMPARE_NO_DIFFERENCE,
	// compare was successfull differences were found
	COMPARE_SUCCESS,
	// compare got an error
	COMPARE_ERROR,
	// merge was successful
	MERGE_SUCCESS,
	// merge got an error
	MERGE_ERROR

}
