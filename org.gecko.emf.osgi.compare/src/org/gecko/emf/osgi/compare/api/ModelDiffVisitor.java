/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.api;

import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * Interface to be used as whiteboard pattern to visit diff-changes of the {@link ModelObjectMerger}
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
public interface ModelDiffVisitor {
	
	  /**
	   * Called for attribute changes 
	   * @param change the attribute change
	   */
	  public void visitAttributeChange(AttributeChange change);
	  
	  /**
	   * Called for reference changes 
	   * @param change the reference change
	   */
	  public void visitReferenceChange(ReferenceChange change);
	  
	  /**
	   * Called, when the visting was finished
	   * @param resource the new {@link Resource}
	   */
	  public void finishedVisiting(Resource resource);

}
