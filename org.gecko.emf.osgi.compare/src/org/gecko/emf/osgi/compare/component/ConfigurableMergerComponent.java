/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.component;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

/**
 * Component that configures the {@link ConfigurableModelObjectMerger} 
 * @author Mark Hoffmann
 * @since 04.11.2019
 */
@Component(immediate=true, configurationPolicy=ConfigurationPolicy.REQUIRE, configurationPid=ModelObjectMerger.FACTORY_ID)
public class ConfigurableMergerComponent {

	private static final Logger logger = Logger.getLogger(ConfigurableMergerComponent.class.getName());
	
	@Reference
	private ConfigurationAdmin configAdmin;
	private volatile Configuration configuration;

	@Activate
	@Modified
	public void active(ModelObjectMergerConfig config) throws ConfigurationException {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		if (config.name() == null || config.name().isEmpty()) {
			throw new ConfigurationException(ModelObjectMerger.MERGER_NAME, "The 'name' property of the configuration is medatory");
		}
		properties.put(ModelObjectMerger.MERGER_NAME, config.name());
		String comparisonTarget = config.comparisonFactoryTarget();
		if (validateFilter(comparisonTarget)) {
			properties.put(ModelObjectMerger.COMPARISON_REFERENCE + ".target", comparisonTarget);
		}
		String visitorTarget = config.visitorTarget();
		if (validateFilter(visitorTarget)) {
			properties.put(ModelObjectMerger.VISITOR_REFERENCE + ".target", visitorTarget);
		}
		try {
			configuration = configAdmin.getFactoryConfiguration(ModelObjectMerger.CONFIGURABLE_FACTORY_ID, config.name(), "?");
			configuration.update(properties);
		} catch (IOException e) {
			logger.log(Level.SEVERE, String.format("[%s] Error creating ModelObjectMerger configuration", config.name()), e);
		}
	}
	
	@Deactivate
	public void deactivate() {
		if (configuration != null) {
			String name = (String) configuration.getProperties().get(ModelObjectMerger.MERGER_NAME);
			try {
				configuration.delete();
			} catch (IOException e) {
				logger.log(Level.SEVERE, String.format("[%s] Error deleting configuration for ModelObjectMerger", name), e);
			}
		}
	}
	
	/**
	 * Validates if the target filter string is a valid LDAP filter
	 * @param filterString the filter string to validate as LDAP filter
	 * @return <code>true</code>, if validation was successful, otherwise <code>false</code>
	 */
	private boolean validateFilter(String filterString) {
		if (filterString == null || filterString.isEmpty()) {
			return false;
		}
		try {
			filterString = filterString.replace("\\(", "(").replace("\\)", ")");
			FrameworkUtil.createFilter(filterString);
			return true;
		} catch (InvalidSyntaxException e) {
			logger.log(Level.SEVERE, String.format("Cannot parse target filter '%s'", filterString), e);
		}
		return false;
	}

	@Modified
	public void modified(ModelObjectMergerConfig config, ComponentContext ctx) {
		
	}
}
