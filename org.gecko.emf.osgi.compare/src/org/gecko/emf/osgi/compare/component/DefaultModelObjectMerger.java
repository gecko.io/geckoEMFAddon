/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.component;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.EMFCompare.Builder;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.diff.DiffBuilder;
import org.eclipse.emf.compare.diff.IDiffEngine;
import org.eclipse.emf.compare.diff.IDiffProcessor;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.merge.BatchMerger;
import org.eclipse.emf.compare.merge.IBatchMerger;
import org.eclipse.emf.compare.merge.IMerger;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.gecko.emf.osgi.compare.api.IgnoreFeaturesDiffEngine;
import org.gecko.emf.osgi.compare.api.ModelCompareResultType;
import org.gecko.emf.osgi.compare.api.ModelDiffVisitor;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;

/**
 * Implementation using EMF Compare to compare and merge model instances
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
public abstract class DefaultModelObjectMerger implements ModelObjectMerger {

	private List<ModelDiffVisitor> modelDiffVisitorList = new CopyOnWriteArrayList<ModelDiffVisitor>();
	private IBatchMerger merger;
	private IComparisonFactory comparisonFactory;

	/**
	 * Compares the two resources and merges them into the right, the old object
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @return <code>true</code>, on success otherwise <code>false</code>
	 */
	public ModelCompareResultType compareAndMerge(Resource resourceNew, Resource resourceOld) {
		return compareAndMerge(resourceNew, resourceOld, null);
	}
	
	/**
	 * Compares the two resources and merges them into the right, the old object
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @param ignoreFeatures a list of {@link EStructuralFeature} to be ignored during compare
	 * @return <code>true</code>, on success otherwise <code>false</code>
	 */
	public ModelCompareResultType compareAndMerge(Resource resourceNew, Resource resourceOld, List<EStructuralFeature> ignoreFeatures) {
		Comparison comparison = compareRaw(resourceNew, resourceOld, ignoreFeatures);
		if (comparison == null) {
			return ModelCompareResultType.COMPARE_ERROR;
		} else {
			if (comparison.getDifferences().isEmpty()) {
				return ModelCompareResultType.COMPARE_NO_DIFFERENCE;
			}
		}
		boolean mergeSuccess = merge(comparison.getDifferences());
		return mergeSuccess ? ModelCompareResultType.MERGE_SUCCESS : ModelCompareResultType.MERGE_ERROR;
	}

	/**
	 * Compares the two resources and returns the changes 
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @return the comparison containing all differences
	 */
	public ModelCompareResultType compare(Resource resourceNew, Resource resourceOld) {
		return compare(resourceNew, resourceOld, null);
	}

	/**
	 * Compares the two resources and returns the changes 
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @param ignoreFeatures a list of {@link EStructuralFeature} to be ignored during compare
	 * @return the comparison containing all differences
	 */
	public ModelCompareResultType compare(Resource resourceNew, Resource resourceOld, List<EStructuralFeature> ignoreFeatures) {
		if (resourceNew == null || resourceOld == null) {
			return ModelCompareResultType.COMPARE_ERROR;
		}
		Comparison comparison = compareRaw(resourceNew, resourceOld, ignoreFeatures);
		if (comparison.getDifferences().isEmpty()) {
			return ModelCompareResultType.COMPARE_NO_DIFFERENCE;
		} else {
			return ModelCompareResultType.COMPARE_SUCCESS;
		}
	}
	
	/**
	 * Compares the two resources and returns the changes 
	 * @param resourceNew the resource with the new object version
	 * @param resourceOld the resource with the old, current object version
	 * @param ignoreFeatures a list of {@link EStructuralFeature} to be ignored during compare
	 * @return the comparison containing all differences
	 */
	public Comparison compareRaw(Resource resourceNew, Resource resourceOld, List<EStructuralFeature> ignoreFeatures) {
		if (resourceNew == null || resourceOld == null) {
			return null;
		}
		EPackage.Registry.INSTANCE.put(EcorePackage.eNS_URI, EcorePackage.eINSTANCE);
		IComparisonScope scope = new DefaultComparisonScope(resourceNew, resourceOld, null);
		IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.NEVER);
		
		IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(matcher, comparisonFactory);
		matchEngineFactory.setRanking(20);
		IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
		matchEngineRegistry.add(matchEngineFactory);
		
		Builder comparatorBuilder = EMFCompare.builder().setMatchEngineFactoryRegistry(matchEngineRegistry);
		if (ignoreFeatures != null && ignoreFeatures.size() > 0) {
			IDiffProcessor diffProcessor = new DiffBuilder();
			IDiffEngine diffEngine = new IgnoreFeaturesDiffEngine(diffProcessor, ignoreFeatures);
			comparatorBuilder.setDiffEngine(diffEngine);
		}
		EMFCompare comparator = comparatorBuilder.build();
		Comparison comparison = comparator.compare(scope);
		for (Diff difference : comparison.getDifferences()) {
			if (difference instanceof AttributeChange) {
				visitAttributeChanges((AttributeChange) difference);
			}
			if (difference instanceof ReferenceChange) {
				visitReferenceChanges((ReferenceChange) difference);
			}
		}
		return comparison;
	}

	/**
	 * Merges differences
	 * @param differences the compare differences to merge
	 * @return <code>true</code>, if merge was successful, otherwise <code>false</code>
	 */
	public boolean merge(List<Diff> differences) {
		try {
			merger.copyAllLeftToRight(differences, null);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Fires finished visiting call to all visitors.
	 * @param resource the changed, merged resource 
	 */
	public void fireVisitorChanges(Resource resource) {
		fireFinishedVisiting(resource);
	}

	abstract protected String getName();

	/**
	 * Activate method called by OSG DS on activation of a component
	 */
	protected void initialize() {
	    // initialize merger
	    IMerger.Registry mergerRegistry = IMerger.RegistryImpl.createStandaloneInstance();
	    merger = new BatchMerger(mergerRegistry);
	}

	/**
	 * Adds a {@link ModelDiffVisitor} instance to the registry list
	 * @param visitor the visitor to add
	 */
	protected void addModelDiffVisitor(ModelDiffVisitor visitor) {
		modelDiffVisitorList.add(visitor);
	}

	/**
	 * Removed a visitor from the registry list
	 * @param visitor the visitor to be removed
	 */
	protected void removeModelDiffVisitor(ModelDiffVisitor visitor) {
		modelDiffVisitorList.remove(visitor);
	}
	
	/**
	 * Sets a {@link IComparisonFactory} instance
	 * @param comparison the the comparison factory instance
	 */
	protected void setComparisonFactory(IComparisonFactory comparison) {
		comparisonFactory = comparison;
	}
	
	/**
	 * Executes the visit for attribute changes 
	 * @param change the attribute change
	 */
	protected synchronized void visitAttributeChanges(AttributeChange change) {
		if (change == null) {
			return;
		}
		for (ModelDiffVisitor visitor : modelDiffVisitorList) {
			visitor.visitAttributeChange(change);
		}
	}

	/**
	 * Executes the visit for reference changes 
	 * @param change the reference change
	 */
	protected synchronized void visitReferenceChanges(ReferenceChange change) {
		if (change == null) {
			return;
		}
		for (ModelDiffVisitor visitor : modelDiffVisitorList) {
			visitor.visitReferenceChange(change);
		}
	}

	/**
	 * Executes the finished visiting call to all visitors
	 * @param resource the new resource
	 */
	protected synchronized void fireFinishedVisiting(Resource resource) {
		for (ModelDiffVisitor visitor : modelDiffVisitorList) {
			visitor.finishedVisiting(resource);
		}
	}

}
