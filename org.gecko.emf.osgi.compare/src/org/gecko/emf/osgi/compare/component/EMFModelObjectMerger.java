/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.component;

import org.eclipse.emf.compare.match.IComparisonFactory;
import org.gecko.emf.osgi.compare.api.ModelDiffVisitor;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * Implementation using EMF Compare to compare and merge model instances
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
@Component(name=EMFModelObjectMerger.NAME, service=ModelObjectMerger.class, immediate=true)
public class EMFModelObjectMerger extends DefaultModelObjectMerger {

	static final String NAME = "EMFModelObjectMerger";
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.compare.component.DefaultModelObjectMerger#getName()
	 */
	@Override
	protected String getName() {
		return NAME;
	}

	/**
	 * Activate method called by OSG DS on activation of a component
	 */
	@Activate
	public void activate() {
		initialize();
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.compare.component.DefaultModelObjectMerger#setComparisonFactory(org.eclipse.emf.compare.match.IComparisonFactory)
	 */
	@Override
	@Reference(name = COMPARISON_REFERENCE, target="(type=default)")
	protected void setComparisonFactory(IComparisonFactory comparison) {
		super.setComparisonFactory(comparison);
	}

	/**
	 * Adds a {@link ModelDiffVisitor} instance to the registry list
	 * @param visitor the visitor to add
	 */
	@Reference(name=ModelObjectMerger.VISITOR_REFERENCE, cardinality=ReferenceCardinality.MULTIPLE, policy=ReferencePolicy.DYNAMIC, unbind="removeModelDiffVisitor", target="(type=default)")
	public void addModelDiffVisitor(ModelDiffVisitor visitor) {
		super.addModelDiffVisitor(visitor);
	}

	/**
	 * Removed a visitor from the registry list
	 * @param visitor the visitor to be removed
	 */
	public void removeModelDiffVisitor(ModelDiffVisitor visitor) {
		super.removeModelDiffVisitor(visitor);
	}

}
