/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.component;

import org.eclipse.emf.compare.match.IComparisonFactory;
import org.gecko.emf.osgi.compare.api.ModelDiffVisitor;
import org.gecko.emf.osgi.compare.api.ModelObjectMerger;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * Implementation using EMF Compare to compare and merge model instances
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
@Component(name=ConfigurableModelObjectMerger.NAME, service=ModelObjectMerger.class, configurationPolicy=ConfigurationPolicy.REQUIRE, configurationPid=ModelObjectMerger.CONFIGURABLE_FACTORY_ID)
public class ConfigurableModelObjectMerger extends DefaultModelObjectMerger {

	static final String NAME = "ConfigurableModelObjectMerger";
	
	@Activate
	public void activate() {
		initialize();
	}
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.compare.component.DefaultModelObjectMerger#setComparisonFactory(org.eclipse.emf.compare.match.IComparisonFactory)
	 */
	@Override
	@Reference(name=ModelObjectMerger.COMPARISON_REFERENCE)
	protected void setComparisonFactory(IComparisonFactory comparison) {
		super.setComparisonFactory(comparison);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.compare.component.DefaultModelObjectMerger#addModelDiffVisitor(org.gecko.emf.osgi.compare.api.ModelDiffVisitor)
	 */
	@Override
	@Reference(name=ModelObjectMerger.VISITOR_REFERENCE, policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.MULTIPLE, unbind="removeModelDiffVisitor")
	protected void addModelDiffVisitor(ModelDiffVisitor visitor) {
		super.addModelDiffVisitor(visitor);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.compare.component.DefaultModelObjectMerger#removeModelDiffVisitor(org.gecko.emf.osgi.compare.api.ModelDiffVisitor)
	 */
	@Override
	protected void removeModelDiffVisitor(ModelDiffVisitor visitor) {
		super.removeModelDiffVisitor(visitor);
	}
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.compare.component.DefaultModelObjectMerger#getName()
	 */
	@Override
	protected String getName() {
		return NAME;
	}


}
