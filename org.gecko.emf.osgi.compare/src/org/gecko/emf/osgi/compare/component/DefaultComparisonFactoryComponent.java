/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.component;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IEqualityHelperFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * A delegate {@link IComparisonFactory} as OSGi service
 * @author Mark Hoffmann
 * @since 04.11.2019
 */
@Component(name="DefaultComparisonFactoryComponent", property="type=default", immediate=true)
public class DefaultComparisonFactoryComponent implements IComparisonFactory {

	@Reference(cardinality=ReferenceCardinality.MANDATORY, policy=ReferencePolicy.STATIC, target="(type=default)")
	private IEqualityHelperFactory equalityHelper;
	private IComparisonFactory delegate;
	
	@Activate
	public void activate() {
		this.delegate = new DefaultComparisonFactory(equalityHelper);
	}
	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.emf.compare.match.IComparisonFactory#createComparison()
	 */
	@Override
	public Comparison createComparison() {
		return delegate.createComparison();
	}

}
