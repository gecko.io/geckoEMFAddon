/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.compare.component;

/**
 * Annotation that handles the model object configuration
 * @author Mark Hoffmann
 * @since 04.11.2019
 */
public @interface ModelObjectMergerConfig {
	
	String name();
	String visitorTarget() default "(type=default)";
	String comparisonFactoryTarget() default "(type=default)";
	boolean prototype() default false;

}
