/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.component;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.qvt.osgi.api.ModelTransformationConstants;
import org.gecko.qvt.osgi.api.ModelTransformationFactory;
import org.gecko.qvt.osgi.api.ModelTransformator;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;
import org.osgi.service.cm.ConfigurationException;


public class ConfigurableTransformationService implements ModelTransformator, ModelTransformationConstants {
	
	public static String FACTORY_ID = "ConfigurableTransformationService";
	
	protected ModelTransformationFactory transformationFactory;

	protected ResourceSetFactory resourceSetFactory;
	
	private BundleContext context;
	
	private ModelTransformator delegate = null;
	
	/**
	 * Called on component activation ot modification
	 * @param context the bundle context
	 * @param properties the component properties 
	 * @throws ConfigurationException
	 */	
	public void activate(BundleContext context, Map<String, Object> properties) throws ConfigurationException {
		this.context = context;
		String transformatorName = (String) properties.get(TRANSFORMATOR_NAME);
		String templatePath = (String) properties.get(TEMPLATE_PATH);
		if (transformatorName == null || 
				templatePath == null) {
			throw new ConfigurationException(TRANSFORMATOR_NAME + "," + TEMPLATE_PATH, "The configuration is incomplete to create a model transformator. At least transformator name or/and template path must be given.");
		}
		if (delegate != null) {
			delegate.dispose();
		}
		try {
			URI templateUri = getTemplateUri(templatePath);
			synchronized (resourceSetFactory) {
				ResourceSet resourceSet = resourceSetFactory.createResourceSet();
				delegate = transformationFactory.createNewModelTransformator(resourceSet, templateUri);
			}
		} catch (URISyntaxException e) {
			throw new ConfigurationException(TEMPLATE_PATH, "The template path is invalid. Cannot load template from this location.");
		}
	}
	
	/**
	 * Called on component de-activation 
	 */
	public void deactivate() {
		delegate.dispose();
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.qvt.osgi.api.ModelTransformator#startTransformations(java.util.List)
	 */
	@Override
	public synchronized List<? extends EObject> startTransformations(List<? extends EObject> inObjects) {
		return delegate.startTransformations(inObjects);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.qvt.osgi.api.ModelTransformator#startTransformation(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public synchronized EObject startTransformation(EObject inObject) {
		return delegate.startTransformation(inObject);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.qvt.osgi.api.ModelTransformator#startTransformation(java.util.List)
	 */
	@Override
	public synchronized EObject startTransformation(List<? extends EObject> inObjects) {
		return delegate.startTransformation(inObjects);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.qvt.osgi.api.ModelTransformator#dispose()
	 */
	@Override
	public void dispose() {
		delegate.dispose();
	}

	/**
	 * Returns the bundle from the given bsn version string.
	 * This parameter is expected in the format:
	 * <bsn>:<version>, where the version part is optional.
	 * @param bsnVersionString the {@link String} in the format from above
	 */
	private Bundle getBundle(String bsnVersionString) {
		String[] bsnVersion = bsnVersionString.split(":");
		String bsn = bsnVersion[0];
		Version version = null;
		if (bsnVersion.length == 2) {
			version = Version.parseVersion(bsnVersion[1]);
		}
		Set<Bundle> candidates = new TreeSet<>(new Comparator<Bundle>() {
	
			/* 
			 * (non-Javadoc)
			 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(Bundle o1, Bundle o2) {
				return o1.getVersion().compareTo(o2.getVersion());
			}
		});
		for (Bundle b : context.getBundles()) {
			if (bsn.equalsIgnoreCase(b.getSymbolicName())) {
				if (version == null) {
					candidates.add(b);
				} else {
					if (b.getVersion().compareTo(version) == 0) {
						return b;
					} else {
						continue;
					}
				}
			}
		}
		if (candidates.isEmpty()) {
			throw new IllegalStateException("There is no bundle with this bsn and version '" + bsn + ":" + version + "'");
		} else {
			return candidates.stream().findFirst().get();
		}
	}

	/**
	 * Loads the template from the given path
	 * @throws URISyntaxException 
	 */
	private URI getTemplateUri(String templatePath) throws URISyntaxException {
		String[] segments = templatePath.split("/");
		if (segments.length < 2) {
			throw new IllegalStateException("There are at least two segments expected in the ecore path");
		}
		Bundle bundle = getBundle(segments[0]);
		String path = templatePath.replace(segments[0], "");
		URL url = bundle.getResource(path);
		if (url == null) {
			throw new IllegalStateException("There was no template found at '" + segments[0] + path + "'");
		}
		java.net.URI uri = url.toURI();
		return URI.createHierarchicalURI(uri.getScheme(), uri.getAuthority(), null, uri.getPath().split("/"), null, null);
	}

}
