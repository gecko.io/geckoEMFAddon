/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.component;

import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.gecko.qvt.osgi.api.ModelTransformationConstants;
import org.gecko.qvt.osgi.api.ModelTransformator;
import org.gecko.qvt.osgi.util.JULLogWriter;

/**
 * QVT Implementation of a model transformator
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
public class QVTModelTransformator implements ModelTransformator, ModelTransformationConstants {

	private static final String VALIDATION_MESSAGE = "%sSource: [%s] Message [%s]";
	private static final Logger logger = Logger.getLogger(QVTModelTransformator.class.getName());
	private final ResourceSet resourceSet;
	private final String templateName;
	private URI templateUri;
	private TransformationExecutor executor = null;
	private ExecutionContextImpl context = null;

	QVTModelTransformator(ResourceSet resourceSet, String templateName) {
		this.resourceSet = resourceSet;
		this.templateName = templateName;
		this.templateUri = null;
	}

	QVTModelTransformator(ResourceSet resourceSet, URI templateUri) {
		this.resourceSet = resourceSet;
		this.templateUri = templateUri;
		this.templateName = null;
	}

	/**
	 * Initializes the transformation engine and does a warm-up for the executor to reduce execution time 
	 */
	void init() {
		if (templateName == null && templateUri == null || resourceSet == null) {
			throw new IllegalArgumentException("Error initializing QVT helper without template or/and resource set");
		}
		if (templateName != null) {
			URL templateUrl = getClass().getResource(templateName);
			templateUri = URI.createURI(templateUrl.toString());
		}
		executor = new TransformationExecutor(templateUri, resourceSet.getPackageRegistry());
		Diagnostic result = executor.loadTransformation();
		if (result.getSeverity() == Diagnostic.OK) {
			context = new ExecutionContextImpl();
			context.setConfigProperty("keepModeling", true);
			// to log from QVTO during development uncomment the line below
			context.setLog(new JULLogWriter("o.e.q.o.qvtTransformatorExecutor"));
		} else {
			String msg = getDiagnosticMessage(result);
			logger.log(Level.SEVERE, String.format("Error loading transformation template: %s", msg));
			throw new IllegalStateException(msg);
		}
	}

	/* (non-Javadoc)
	 * @see com.swarco.common.mmt.ModelTransformator#startTransformations(java.util.List)
	 */
	@Override
	public List<? extends EObject> startTransformations(List<? extends EObject> inObjects) {
		if (inObjects == null || resourceSet == null) {
			throw new IllegalStateException("Error transforming object with null instance or no resource set");
		}
		try {
			if (executor == null) {
				init();
			}
			// create the input extent with its initial contents
			ModelExtent input = new BasicModelExtent(inObjects);    
			// create an empty extent to catch the output
			ModelExtent output = new BasicModelExtent();
			ExecutionDiagnostic result = executor.execute(context, input, output);
			if(result.getSeverity() == Diagnostic.OK) {
				// the output objects got captured in the output extent
				List<? extends EObject> outObjects = output.getContents();
				logger.fine("QVT transformation succeeded with: " + outObjects.size() + " elements");
				return outObjects;
			} else {
				String message = getDiagnosticMessage(result);
				throw new IllegalStateException(String.format("Error executing transformation because of diagnostic errors: %s", message));
			}
		} catch (Exception e) {
			throw new IllegalStateException("Error transforming model from " + inObjects.toString(), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.swarco.common.mmt.ModelTransformator#startTransformations(java.util.List)
	 */
	@Override
	public EObject startTransformation(List<? extends EObject> inObjects) {
		if (inObjects == null || resourceSet == null) {
			throw new IllegalStateException("Error transforming object with null instance or no resource set");
		}
		try {
			if (executor == null) {
				init();
			}
			// create the input extent with its initial contents
			ModelExtent input = new BasicModelExtent(inObjects);    
			// create an empty extent to catch the output
			ModelExtent output = new BasicModelExtent();
			ExecutionDiagnostic result = executor.execute(context, input, output);
			if(result.getSeverity() == Diagnostic.OK) {
				// the output objects got captured in the output extent
				List<EObject> outObjects = output.getContents();

				if (outObjects.size() > 0) {
					logger.fine("QVT transformation succeeded with: " + outObjects.size() + " elements");
					return outObjects.get(0);
				}
			} else {
				String message = getDiagnosticMessage(result);
				throw new IllegalStateException(String.format("Error executing transformation because of diagnostic errors: %s", message));
			}
		} catch (Exception e) {
			if (e instanceof IllegalStateException) {
				throw e;
			}
			throw new IllegalStateException("Error transforming model from " + inObjects.toString(), e);
		}
		throw new IllegalStateException("Transformation failed with no result object");
	}

	/* (non-Javadoc)
	 * @see com.swarco.common.mmt.ModelTransformator#startTransformation(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public EObject startTransformation(EObject inObject) {
		if (inObject == null || resourceSet == null) {
			throw new IllegalStateException("Error transforming object with null instance or no resource set");
		}
		try {
			if (executor == null) {
				init();
			}
			// create the input extent with its initial contents
			ModelExtent input = new BasicModelExtent(ECollections.singletonEList(inObject));    
			// create an empty extent to catch the output
			ModelExtent output = new BasicModelExtent();
			ExecutionDiagnostic result = executor.execute(context, input, output);
			if(result.getSeverity() == Diagnostic.OK) {
				// the output objects got captured in the output extent
				List<EObject> outObjects = output.getContents();

				if (outObjects.size() > 0) {
					logger.fine("QVT transformation succeeded with: " + outObjects.size() + " elements");
					return outObjects.get(0);
				}
			} else {
				String message = getDiagnosticMessage(result);
				throw new IllegalStateException(String.format("Error executing transformation because of diagnostic errors: %s", message));
			}
		} catch (Exception e) {
			throw new IllegalStateException("Error transforming model from " + inObject.eClass().getName(), e);
		}
		throw new IllegalStateException("Transformation failed with no result object");
	}

	/* (non-Javadoc)
	 * @see com.swarco.common.mmt.ModelTransformator#dispose()
	 */
	@Override
	public void dispose() {
		if (executor != null) {
			executor = null;
		}
		if (context != null) {
			context = null;
		}
	}
	
    /**
     * Returns a message for a diagnostic
     * @param diagnostic the {@link Diagnostic}
     */
    private String getDiagnosticMessage(Diagnostic diagnostic) {
        StringBuilder message = new StringBuilder();
        createValidationMessage("", diagnostic, message);
        return message.toString();
    }
    
    private void createValidationMessage(String indent, Diagnostic diagnostic, StringBuilder message) {
    	String separator = System.getProperty("line.separator");
        message.append(String.format(VALIDATION_MESSAGE, indent, diagnostic.getSource(), diagnostic.getMessage()));
        message.append(separator);
        diagnostic.getChildren().forEach(d -> createValidationMessage("  " + indent , d, message));
    }

}
