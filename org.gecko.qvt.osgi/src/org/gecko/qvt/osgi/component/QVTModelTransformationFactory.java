/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.m2m.qvt.oml.TransformationExecutor.BlackboxRegistry;
import org.gecko.core.pool.Pool;
import org.gecko.emf.osgi.annotation.require.RequireEMF;
import org.gecko.qvt.osgi.api.ModelTransformationConstants;
import org.gecko.qvt.osgi.api.ModelTransformationFactory;
import org.gecko.qvt.osgi.api.ModelTransformator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Factory implementation for {@link QVTModelTransformator} instances
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
@RequireEMF
@Component(name="QVTModelTransformationFactory",immediate=true, service=ModelTransformationFactory.class)
public class QVTModelTransformationFactory implements ModelTransformationFactory, ModelTransformationConstants {

	private final Logger logger = Logger.getLogger(QVTModelTransformationFactory.class.getName());
	private Map<URI, ModelTransformator> transformatorCache = new ConcurrentHashMap<URI, ModelTransformator>();
	private volatile ServiceTracker<?, ?> blackboxTracker;

	/* (non-Javadoc)
	 * @see com.swarco.common.mmt.ModelTransformationFactory#createModelTransformator(org.eclipse.emf.ecore.resource.ResourceSet, org.eclipse.emf.common.util.URI)
	 */
	@Override
	public ModelTransformator createModelTransformator(ResourceSet resourceSet, URI templateUri) {
		ModelTransformator transformator = null;
		if (!transformatorCache.containsKey(templateUri)) {
			ModelTransformator newTransformator = createNewModelTransformator(resourceSet, templateUri);
			transformatorCache.put(templateUri, newTransformator);
			transformator = newTransformator;
		} else {
			transformator = transformatorCache.get(templateUri);
		}
		return transformator;
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.qvt.osgi.api.ModelTransformationFactory#createNewModelTransformator(org.eclipse.emf.ecore.resource.ResourceSet, org.eclipse.emf.common.util.URI)
	 */
	@Override
	public ModelTransformator createNewModelTransformator(ResourceSet resourceSet, URI templateUri) {
		QVTModelTransformator transformator = new QVTModelTransformator(resourceSet, templateUri);
		transformator.init();
		return transformator;
	}


	/* 
	 * (non-Javadoc)
	 * @see org.gecko.qvt.osgi.api.ModelTransformationFactory#createNewModelTransformatorPool(org.eclipse.emf.ecore.resource.ResourceSet, org.eclipse.emf.common.util.URI, int, long)
	 */
	@Override
	public Pool<ModelTransformator> createNewModelTransformatorPool(ResourceSet resourceSet, URI templateUri, int poolSize, long poolTimeout) {
		Pool<ModelTransformator> pool = new Pool<ModelTransformator>("Modeltransformator Tool: " + templateUri, 
				() -> {
					return createNewModelTransformator(resourceSet, templateUri);
				}, 
				t -> {}, 
				poolSize, 
				poolTimeout);
		pool.initialize();
		return pool;
	}
	
	/**
	 * Called on component activation
	 * @param context the bundle context
	 */
	@Activate
	public void activate(BundleContext context) {
		try {
			Filter bbFilter = FrameworkUtil.createFilter("(" + QVT_BLACKBOX + "=true)");
			BlackboxRegistry blackboxRegistry = BlackboxRegistry.INSTANCE;
			blackboxTracker = new ServiceTracker<Object, Object>(context, bbFilter, null) {
				
				/* 
				 * (non-Javadoc)
				 * @see org.osgi.util.tracker.ServiceTracker#addingService(org.osgi.framework.ServiceReference)
				 */
				@Override
				public Object addingService(ServiceReference<Object> reference) {
					String moduleName = (String) reference.getProperty(BLACKBOX_MODULENAME);
					String uqn = (String) reference.getProperty(BLACKBOX_QUALIFIED_UNIT_NAME);
					Object blackbox = context.getService(reference);
					if (moduleName == null && uqn != null) {
						moduleName = blackbox.getClass().getSimpleName();
					}
					if (uqn != null) {
						blackboxRegistry.registerModule(blackbox.getClass(), uqn, moduleName);
					} else {
						blackboxRegistry.registerModule(blackbox.getClass());
					}
					return blackbox;
				}
			};
			blackboxTracker.open();
		} catch (InvalidSyntaxException e) {
			logger.log(Level.SEVERE, "Cannot open tracker to track blackboxes, because of wrong filter", e);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "An error occured trying to track blackbox services", e);
		}
	}

	/**
	 * Deactivate callback, used by OSGi DS
	 */
	@Deactivate
	public void deactivate() {
		if (blackboxTracker != null) {
			blackboxTracker.close();
		}
		transformatorCache.forEach((k,v)->v.dispose());
		transformatorCache.clear();
	}

}
