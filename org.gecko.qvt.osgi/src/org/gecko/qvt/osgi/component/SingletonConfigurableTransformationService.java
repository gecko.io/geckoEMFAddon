/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.component;

import java.util.Map;

import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.qvt.osgi.api.ModelTransformationConstants;
import org.gecko.qvt.osgi.api.ModelTransformationFactory;
import org.gecko.qvt.osgi.api.ModelTransformator;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

/**
 * 
 * @author ilenia
 * @since Dec 12, 2019
 */
@Component(configurationPolicy=ConfigurationPolicy.REQUIRE, 
configurationPid=ModelTransformationConstants.CONFIGURABLE_TRANSFORMATOR_FACTORY_ID, 
service=ModelTransformator.class,
reference = {
		@Reference(service = ModelTransformationFactory.class, name = "transformationFactory", field = "transformationFactory"),
		@Reference(service = ResourceSetFactory.class, name = "qvt.model", field = "resourceSetFactory")
},
scope = ServiceScope.SINGLETON)
public class SingletonConfigurableTransformationService extends ConfigurableTransformationService {
	
	/**
	 * Called on component activation and modification
	 * @param context the bundle context
	 * @param properties the component properties 
	 * @throws ConfigurationException
	 */
	@Activate
	@Modified
	public void activate(BundleContext context, Map<String, Object> properties) throws ConfigurationException {
		super.activate(context, properties);
	}
	
	/**
	 * Called on component de-activation 
	 */
	@Deactivate
	public void deactivate() {
		super.deactivate();
	}
	
}
