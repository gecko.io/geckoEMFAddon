/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.helper;

import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.List;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.WriterLog;

/**
 * Helper to start the transformation for a given template using the given resource set package registry.
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
public class QVTHelper {

  private final ResourceSet resourceSet;
  private final String templateName;
  private URI templateUri;
  private TransformationExecutor executor = null;
  private ExecutionContextImpl context = null;

  public QVTHelper(ResourceSet resourceSet, String templateName) {
    this.resourceSet = resourceSet;
    this.templateName = templateName;
    this.templateUri = null;
  }
  
  public QVTHelper(ResourceSet resourceSet, URI templateUri) {
	  this.resourceSet = resourceSet;
	  this.templateUri = templateUri;
	  this.templateName = null;
  }

  /**
   * Initializes the transformation engine and does a warm-up for the executor to reduce execution time 
   */
  public void init() {
	
    if (templateName == null && templateUri == null || resourceSet == null) {
      throw new IllegalArgumentException("Error initializing QVT helper without template or/and resource set");
    }
    if (templateName != null) {
    	URL templateUrl = getClass().getResource(templateName);
    	templateUri = URI.createURI(templateUrl.toString());
    }
    executor = new TransformationExecutor(templateUri, resourceSet.getPackageRegistry());
    executor.loadTransformation();
    context = new ExecutionContextImpl();
    context.setConfigProperty("keepModeling", true);
    // to log from QVTO during development uncomment the line below
    context.setLog(new WriterLog(new OutputStreamWriter(System.out)));
  }

  /**
   * Starts the transformation programmatic 
   * http://wiki.eclipse.org/QVTOML/Examples/InvokeInJava
   * https://www.eclipse.org/forums/index.php/t/853024/
   */
  public EObject startTransformation(EObject inObject) {
    if (inObject == null || resourceSet == null) {
      throw new IllegalStateException("Error transforming object with null instance or no resource set");
    }
    try {
      if (executor == null) {
        init();
      }
      // create the input extent with its initial contents
      ModelExtent input = new BasicModelExtent(ECollections.singletonEList(inObject));    
      // create an empty extent to catch the output
      ModelExtent output = new BasicModelExtent();
      ExecutionDiagnostic result = executor.execute(context, input, output);
      if(result.getSeverity() == Diagnostic.OK) {
        // the output objects got captured in the output extent
        List<EObject> outObjects = output.getContents();
  
        if (outObjects.size() > 0) {
          return outObjects.get(0);
        }
      } else {
        throw new IllegalStateException("Error executing transformation because of diagnostic errors: " + result.toString());
      }
    } catch (Exception e) {
      throw new IllegalStateException("Error transforming model from " + inObject.eClass().getName(), e);
    }
    throw new IllegalStateException("Transformation failed with no result object");
  }
  
  /**
   * Starts the transformation programmatic 
   * http://wiki.eclipse.org/QVTOML/Examples/InvokeInJava
   * https://www.eclipse.org/forums/index.php/t/853024/
   */
  public EObject startTransformation(List<? extends EObject> inObjects) {
    if (inObjects == null || resourceSet == null) {
      throw new IllegalStateException("Error transforming object with null instance or no resource set");
    }
    try {
      if (executor == null) {
        init();
      }
      // create the input extent with its initial contents
      ModelExtent input = new BasicModelExtent(inObjects);    
      // create an empty extent to catch the output
      ModelExtent output = new BasicModelExtent();
      ExecutionDiagnostic result = executor.execute(context, input, output);
      if(result.getSeverity() == Diagnostic.OK) {
	      // the output objects got captured in the output extent
	      List<EObject> outObjects = output.getContents();
	
	      if (outObjects.size() > 0) {
	        return outObjects.get(0);
	      }
      } else {
        throw new IllegalStateException("Error executing transformation because of diagnostic errors: " + result.toString());
      }
    } catch (Exception e) {
      throw new IllegalStateException("Error transforming model from " + inObjects.toString(), e);
    }
    throw new IllegalStateException("Transformation failed with no result object");
  }
  
  /**
   * Starts the batch transformation programmatic 
   * http://wiki.eclipse.org/QVTOML/Examples/InvokeInJava
   * https://www.eclipse.org/forums/index.php/t/853024/
   */
  public List<? extends EObject> startTransformations(List<? extends EObject> inObjects) {
    if (inObjects == null || resourceSet == null) {
      throw new IllegalStateException("Error transforming object with null instance or no resource set");
    }
    try {
      if (executor == null) {
        init();
      }
      // create the input extent with its initial contents
      ModelExtent input = new BasicModelExtent(inObjects);    
      // create an empty extent to catch the output
      ModelExtent output = new BasicModelExtent();
      ExecutionDiagnostic result = executor.execute(context, input, output);
      if(result.getSeverity() == Diagnostic.OK) {
        // the output objects got captured in the output extent
        List<? extends EObject> outObjects = output.getContents();
        return outObjects;
      } else {
        throw new IllegalStateException("Error executing transformation because of diagnostic errors: " + result.toString());
      }
    } catch (Exception e) {
      throw new IllegalStateException("Error transforming model from " + inObjects.toString(), e);
    }
  }

  /**
   * Release the resources
   */
  public void dispose() {
    if (executor != null) {
      executor = null;
    }
    if (context != null) {
      context = null;
    }
  }

}
