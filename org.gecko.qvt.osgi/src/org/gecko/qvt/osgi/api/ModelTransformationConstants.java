/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.api;

/**
 * Constants for registering blackboxes
 * @author Mark Hoffmann
 * @since 12.11.2017
 */
public interface ModelTransformationConstants {
	
	/*
	 * The property name of the blackbox registered service
	 */
	public static final String QVT_BLACKBOX = "qvt.blackbox";
	/*
	 * The name of the Blackbox
	 */
	public static final String BLACKBOX_MODULENAME = "qvt.blackbox.moduleName";
	/*
	 * The name to use as import in qvto templates:
	 * Usually this is the full qualified class name of the Blackbox class
	 */
	public static final String BLACKBOX_QUALIFIED_UNIT_NAME = "qvt.blackbox.unitQualifiedName";
	/*
	 * The name of a configurable configuration
	 */
	public static final String TRANSFORMATOR_NAME = "qvt.transformatorName";
	/*
	 * The path to a template qvto file in style like this <bsn>:<version>/<path-to-file>.qvto
	 */
	public static final String TEMPLATE_PATH = "qvt.templatePath";
	/*
	 * The target for the needed models in ldap style e.g. (&(emf.model.name=modelA)(emf.model.name=modelB))
	 */
	public static final String MODEL_TARGET = "qvt.model.target";
	/*
	 * The factory id of configurable transformator factory
	 */
	public static final String CONFIGURABLE_TRANSFORMATOR_FACTORY_ID = "ConfigurableTransformationService";	
	/*
	 * The factory id of prototype configurable transformator factory
	 */
	public static final String PROTOTYPE_CONFIGURABLE_TRANSFORMATOR_FACTORY_ID = "PrototypeConfigurableTransformationService";
	/*
	 * The factory id of the configurable pool model transformator factory
	 */
	public static final String CONFIGURABLE_POOL_TRANSFORMATOR_FACTORY_ID = "ConfigurableModelTransformatorPool";

}
