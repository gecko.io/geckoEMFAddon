/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.api;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.core.pool.Pool;

/**
 * Service interface for a factory that creates model tranformators.
 * @author Mark Hoffmann
 * @since 20.10.2017
 */
public interface ModelTransformationFactory {
	
	/**
	 * Creates a model transformator for the given {@link ResourceSet} and template URI
	 * @param resourceSet the {@link ResourceSet} to be used
	 * @param templateUri the URI to the qvt-templates
	 * @return an instance of the model transformator
	 */
	public ModelTransformator createModelTransformator(ResourceSet resourceSet, URI templateUri);
	
	/**
	 * Creates a new model transformator for the given {@link ResourceSet} and template URI
	 * @param resourceSet the {@link ResourceSet} to be used
	 * @param templateUri the URI to the qvt-templates
	 * @return an instance of the model transformator
	 */
	public ModelTransformator createNewModelTransformator(ResourceSet resourceSet, URI templateUri);

	/**
	 * Creates a new model transformator pool for the given {@link ResourceSet} and template URI
	 * @param resourceSet the {@link ResourceSet} to be used
	 * @param templateUri the URI to the qvt-templates
	 * @param poolSize the amount of ModelTransformators in the Pool
	 * @param poolTimeout the default Milliseconds to wait until a pool poll times out  
	 * @return an instance of the model transformator
	 */
	public Pool<ModelTransformator> createNewModelTransformatorPool(ResourceSet resourceSet, URI templateUri, int poolSize, long poolTimeout);

}
