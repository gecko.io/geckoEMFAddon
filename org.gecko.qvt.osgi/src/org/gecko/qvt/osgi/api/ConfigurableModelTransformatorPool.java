/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.qvt.osgi.api;

import java.util.Map;

import org.gecko.core.pool.ConfigurablePoolComponent;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentServiceObjects;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ServiceScope;

/**
 * Implementation of the ConfigurablePoolComponent for ModelTransformator
 * Every time a ModelTransformator is registered, it registers a Pool<ModelTransformator> as a 
 * service (if the corresponding property is set to <code>true<code>), otherwise it just creates the 
 * pool and saves it in a map.
 * 
 * @author ilenia
 * @since Dec 13, 2019
 */
@Component(configurationPolicy=ConfigurationPolicy.REQUIRE, 
configurationPid=ModelTransformationConstants.CONFIGURABLE_POOL_TRANSFORMATOR_FACTORY_ID, 
scope = ServiceScope.SINGLETON, service = ConfigurableModelTransformatorPool.class)
public class ConfigurableModelTransformatorPool extends ConfigurablePoolComponent<ModelTransformator> {
	
	@Activate
	@Modified
	public void activate(BundleContext ctx, PoolConfiguration config) throws ConfigurationException {
		super.activate(ctx, config);
	}
	
	@Deactivate
	public void deactivate() {
		super.deactivate();
	}
	
	@Reference(name = "poolRef", cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, unbind = "unregisterPool", 
			updated = "registerPool")
	public void registerPool(ComponentServiceObjects<ModelTransformator> serviceObj, Map<String, Object> properties) {
		super.registerPool(serviceObj, properties);
	}
	
	public void unregisterPool(ComponentServiceObjects<ModelTransformator> serviceObj) {
		super.unregisterPool(serviceObj);
	}
}
